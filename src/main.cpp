#include <ceres/ceres.h>
// internal ceres files

#include <petsc.h>
#include <petsc/private/petscimpl.h>
#include <petsc/private/logimpl.h>
#include <petsctao.h>
#include "optional.hpp"

#include <unistd.h>

#include "jacobian_constructor.hpp"
#include "common.hpp"

#include <fstream>

using namespace ceres;
using namespace ceres::examples;
using namespace ceres::internal;


struct CSVPrinter {
  CSVPrinter(const std::string& filepath, const ceres::examples::BALProblem& problem, const std::string& loss, int rank) :
    ofs(filepath),
    loss(loss),
    num_cameras(problem.num_cameras()),
    num_points(problem.num_points()),
    rank(rank)
  {}
  std::ofstream ofs;
  std::string loss;
  int64_t num_cameras;
  int64_t num_points;
  int rank;

  PetscErrorCode operator()(Tao tao) {
    PetscErrorCode ierr;
    PetscFunctionBeginUser;

    PetscInt iteration;
    ierr = TaoGetIterationNumber(tao, &iteration);CHKERRQ(ierr);
    PetscReal cost;
    ierr = TaoGetObjective(tao, &cost);CHKERRQ(ierr);
    PetscReal *obj,*gnorm;
    PetscInt nhist;
    ierr = TaoGetConvergenceHistory(tao,&obj,&gnorm,NULL,NULL,&nhist);CHKERRQ(ierr);
    PetscReal radius;
    ierr = TaoGetCurrentTrustRegionRadius(tao,&radius);CHKERRQ(ierr);
    PetscReal eta;
    ierr = PetscOptionsGetReal(NULL, NULL, "-fieldsplit_camera_ksp_rtol", &eta, NULL);CHKERRQ(ierr);
    char solver[PETSC_MAX_PATH_LEN];
    solver[0] = '\0';
    ierr = PetscOptionsGetString(NULL, NULL, "-fieldsplit_camera_pc_type", solver, PETSC_MAX_PATH_LEN, NULL);CHKERRQ(ierr);

    PC schur;
    KSP ksp;
    PetscInt lits=0;
    if(iteration > 0) {
      ierr = TaoGetKSP(tao, &ksp);CHKERRQ(ierr);
      ierr = KSPGetPC(ksp, &schur);CHKERRQ(ierr);
      PetscInt nf;
      KSP *ksps;
      ierr = PCFieldSplitGetSubKSP(schur,&nf,&ksps);CHKERRQ(ierr);
      ierr = KSPGetIterationNumber(ksps[1], &lits);CHKERRQ(ierr);
    }

    PetscReal inner_solve_flops = -1;
    PetscReal inner_solve_time = -1;
    PetscReal solve_flops = -1;
    PetscReal solve_time = -1;
    PetscLogDouble minf, maxf, totf, ratf, mint, maxt, tott, ratt, ratC, totm, totml, totr, mal, malmax, emalmax, flopr, mem;
    PetscMPIInt        minC, maxC;
    const char         *name;
    PetscLogDouble     zero       = 0.0;
    PetscStageLog stageLog;
    PetscInt numStages,stage,numEvents,event;
    PetscBool          *localStageUsed,    *stageUsed;
    PetscBool          *localStageVisible, *stageVisible;
    PetscStageInfo     *stageInfo = NULL;
    PetscEventPerfInfo *eventInfo = NULL;
    PetscClassPerfInfo *classInfo;
    MPI_Comm comm = PetscObjectComm((PetscObject)tao);
    ierr = PetscLogGetStageLog(&stageLog);CHKERRQ(ierr);
    ierr = MPIU_Allreduce(&stageLog->numStages, &numStages, 1, MPI_INT, MPI_MAX, comm);CHKERRQ(ierr);
    ierr = PetscMalloc1(numStages, &localStageUsed);CHKERRQ(ierr);
    ierr = PetscMalloc1(numStages, &stageUsed);CHKERRQ(ierr);
    ierr = PetscMalloc1(numStages, &localStageVisible);CHKERRQ(ierr);
    ierr = PetscMalloc1(numStages, &stageVisible);CHKERRQ(ierr);
    if (numStages > 0) {
      stageInfo = stageLog->stageInfo;
      for (stage = 0; stage < numStages; stage++) {
        if (stage < stageLog->numStages) {
          localStageUsed[stage]    = stageInfo[stage].used;
          localStageVisible[stage] = stageInfo[stage].perfInfo.visible;
        } else {
          localStageUsed[stage]    = PETSC_FALSE;
          localStageVisible[stage] = PETSC_TRUE;
        }
      }
      ierr = MPIU_Allreduce(localStageUsed,    stageUsed,    numStages, MPIU_BOOL, MPI_LOR,  comm);CHKERRQ(ierr);
      ierr = MPIU_Allreduce(localStageVisible, stageVisible, numStages, MPIU_BOOL, MPI_LAND, comm);CHKERRQ(ierr);
      ierr = MPIU_Allreduce(&stageLog->numStages, &numStages, 1, MPI_INT, MPI_MAX, comm);CHKERRQ(ierr);
    }
    for (stage=0; stage<numStages; stage++) {
      ierr = MPIU_Allreduce(&stageLog->stageInfo[stage].eventLog->numEvents, &numEvents, 1, MPI_INT, MPI_MAX, comm);CHKERRQ(ierr);
      for (event = 0; event < numEvents; event++) {
        eventInfo = stageLog->stageInfo[stage].eventLog->eventInfo;
        if (localStageUsed[stage] && (event < stageLog->stageInfo[stage].eventLog->numEvents) && (eventInfo[event].depth == 0)) {
          if ((eventInfo[event].count > 0) && (eventInfo[event].time > 0.0)) flopr = eventInfo[event].flops; else flopr = 0.0;
          ierr = MPI_Allreduce(&flopr,                          &minf,  1, MPIU_PETSCLOGDOUBLE, MPI_MIN, comm);CHKERRQ(ierr);
          ierr = MPI_Allreduce(&flopr,                          &maxf,  1, MPIU_PETSCLOGDOUBLE, MPI_MAX, comm);CHKERRQ(ierr);
          ierr = MPI_Allreduce(&eventInfo[event].flops,         &totf,  1, MPIU_PETSCLOGDOUBLE, MPI_SUM, comm);CHKERRQ(ierr);
          ierr = MPI_Allreduce(&eventInfo[event].time,          &mint,  1, MPIU_PETSCLOGDOUBLE, MPI_MIN, comm);CHKERRQ(ierr);
          ierr = MPI_Allreduce(&eventInfo[event].time,          &maxt,  1, MPIU_PETSCLOGDOUBLE, MPI_MAX, comm);CHKERRQ(ierr);
          ierr = MPI_Allreduce(&eventInfo[event].time,          &tott,  1, MPIU_PETSCLOGDOUBLE, MPI_SUM, comm);CHKERRQ(ierr);
          ierr = MPI_Allreduce(&eventInfo[event].numMessages,   &totm,  1, MPIU_PETSCLOGDOUBLE, MPI_SUM, comm);CHKERRQ(ierr);
          ierr = MPI_Allreduce(&eventInfo[event].messageLength, &totml, 1, MPIU_PETSCLOGDOUBLE, MPI_SUM, comm);CHKERRQ(ierr);
          ierr = MPI_Allreduce(&eventInfo[event].numReductions, &totr,  1, MPIU_PETSCLOGDOUBLE, MPI_SUM, comm);CHKERRQ(ierr);
          ierr = MPI_Allreduce(&eventInfo[event].count,         &minC,  1, MPI_INT,             MPI_MIN, comm);CHKERRQ(ierr);
          ierr = MPI_Allreduce(&eventInfo[event].count,         &maxC,  1, MPI_INT,             MPI_MAX, comm);CHKERRQ(ierr);
          if (PetscLogMemory) {
            ierr  = MPI_Allreduce(&eventInfo[event].memIncrease,    &mem,   1, MPIU_PETSCLOGDOUBLE, MPI_SUM, comm);CHKERRQ(ierr);
            ierr  = MPI_Allreduce(&eventInfo[event].mallocSpace,    &mal,   1, MPIU_PETSCLOGDOUBLE, MPI_SUM, comm);CHKERRQ(ierr);
            ierr  = MPI_Allreduce(&eventInfo[event].mallocIncrease, &malmax,1, MPIU_PETSCLOGDOUBLE, MPI_SUM, comm);CHKERRQ(ierr);
            ierr  = MPI_Allreduce(&eventInfo[event].mallocIncreaseEvent, &emalmax,1, MPIU_PETSCLOGDOUBLE, MPI_SUM, comm);CHKERRQ(ierr);
          }
          name = stageLog->eventLog->eventInfo[event].name;
        } else {
          flopr = 0.0;
          ierr  = MPI_Allreduce(&flopr,                         &minf,  1, MPIU_PETSCLOGDOUBLE, MPI_MIN, comm);CHKERRQ(ierr);
          ierr  = MPI_Allreduce(&flopr,                         &maxf,  1, MPIU_PETSCLOGDOUBLE, MPI_MAX, comm);CHKERRQ(ierr);
          ierr  = MPI_Allreduce(&zero,                          &totf,  1, MPIU_PETSCLOGDOUBLE, MPI_SUM, comm);CHKERRQ(ierr);
          ierr  = MPI_Allreduce(&zero,                          &mint,  1, MPIU_PETSCLOGDOUBLE, MPI_MIN, comm);CHKERRQ(ierr);
          ierr  = MPI_Allreduce(&zero,                          &maxt,  1, MPIU_PETSCLOGDOUBLE, MPI_MAX, comm);CHKERRQ(ierr);
          ierr  = MPI_Allreduce(&zero,                          &tott,  1, MPIU_PETSCLOGDOUBLE, MPI_SUM, comm);CHKERRQ(ierr);
          ierr  = MPI_Allreduce(&zero,                          &totm,  1, MPIU_PETSCLOGDOUBLE, MPI_SUM, comm);CHKERRQ(ierr);
          ierr  = MPI_Allreduce(&zero,                          &totml, 1, MPIU_PETSCLOGDOUBLE, MPI_SUM, comm);CHKERRQ(ierr);
          ierr  = MPI_Allreduce(&zero,                          &totr,  1, MPIU_PETSCLOGDOUBLE, MPI_SUM, comm);CHKERRQ(ierr);
          ierr  = MPI_Allreduce(&ierr,                          &minC,  1, MPI_INT,             MPI_MIN, comm);CHKERRQ(ierr);
          ierr  = MPI_Allreduce(&ierr,                          &maxC,  1, MPI_INT,             MPI_MAX, comm);CHKERRQ(ierr);
          if (PetscLogMemory) {
            ierr  = MPI_Allreduce(&zero,                        &mem,    1, MPIU_PETSCLOGDOUBLE, MPI_SUM, comm);CHKERRQ(ierr);
            ierr  = MPI_Allreduce(&zero,                        &mal,    1, MPIU_PETSCLOGDOUBLE, MPI_SUM, comm);CHKERRQ(ierr);
            ierr  = MPI_Allreduce(&zero,                        &malmax, 1, MPIU_PETSCLOGDOUBLE, MPI_SUM, comm);CHKERRQ(ierr);
            ierr  = MPI_Allreduce(&zero,                        &emalmax,1, MPIU_PETSCLOGDOUBLE, MPI_SUM, comm);CHKERRQ(ierr);
          }
          name  = "";
        }

        // TODO: how does the setup time work?
        if(std::string(name) == "KSPSolve_FS_Schu") {
          inner_solve_time = maxt;
          inner_solve_flops = totf;
        }
        if(std::string(name) == "KSPSolve") {
          solve_time = maxt;
          solve_flops = totf;
        }
      }
    }

    PetscMPIInt comm_size;
    MPI_Comm_size(PetscObjectComm((PetscObject)tao),&comm_size);

    if(rank == 0) {
      if (iteration == 0) {
        ofs << "problem,solver,cost,gradient_norm_l2,damping,iters,cumulative_inner_solve_time,eta,cameras,points,cumulative_inner_solve_flops,loss,cumulative_solve_time,cumulative_solve_flops,nodes" << std::endl;
      }
      ofs << iteration << ","
        << std::string(solver) << ","
        << cost << ","
        << gnorm[nhist-1] << ","
        << 1.0/radius << ","
        << lits << ","
        << inner_solve_time << ","
        << eta << ","
        << num_cameras << ","
        << num_points << ","
        << inner_solve_flops << ","
        << loss << ","
        << solve_time << ","
        << solve_flops << ","
        << comm_size
        << std::endl;
    }

    // TODO: reset log

    PetscFunctionReturn(0);
  }
};

PetscErrorCode csv_monitor(Tao tao, void* ctx) {
  CSVPrinter* printer = (CSVPrinter*)ctx;
  return (*printer)(tao);
}

PetscErrorCode csv_monitor_destroy(void** ctx) {
  CSVPrinter* printer = (CSVPrinter*)*ctx;
  delete printer;
  ctx = NULL;
  return 0;
}


/*
Options to use with fieldsplit:
  -tao_ntr_pc_type petsc # use petsc preconditioner
  -pc_type fieldsplit # use fieldsplit
  -pc_fieldsplit_type schur # use schur complement
  -fieldsplit_camera_mat_schur_complement_ainv_type blockdiag # use block
diagonal inverse for A00. This is confusing as A00 is the point block
  -pc_fieldsplit_schur_precondition self # matrix provided to the preconditioner
is whole shur complent (use selfp to actually create said matrix). self should
probably be used with -fieldsplit_point_pc_type bjacobi
  -fieldsplit_camera_ksp_type cg # ksp used with the schur complement
  -fieldsplit_camera_pc_type jacobi # pc used with the schur complement
*/

int main(int argc, char** argv) {
  PetscErrorCode ierr;
  PetscInitialize(&argc, &argv, NULL, NULL);
  FLAGS_logtostderr = 1;
  google::InitGoogleLogging(argv[0]);

  char filename[PETSC_MAX_PATH_LEN];
  filename[0] = '\0';
  PetscBool bal;
  PetscBool use_ceres = PETSC_FALSE;
  PetscBool robust = PETSC_FALSE;
  char bal_out[PETSC_MAX_PATH_LEN];
  bal_out[0] = '\0';
  PetscBool write_bal_out = PETSC_FALSE;
  char bal_initial[PETSC_MAX_PATH_LEN];
  bal_initial[0] = '\0';
  PetscBool write_bal_initial = PETSC_FALSE;
  char initial_solution[PETSC_MAX_PATH_LEN];
  initial_solution[0] = '\0';
  PetscBool initial_solution_flag = PETSC_FALSE;
  char csv[PETSC_MAX_PATH_LEN];
  csv[0] = '\0';
  PetscBool csv_flag = PETSC_FALSE;
  PetscReal rot_sigma = 0.0;
  PetscReal trans_sigma = 0.0;
  PetscReal point_sigma = 0.0;
  PetscBool verify = PETSC_FALSE;
  PetscBool use_ceres_direct = PETSC_FALSE;
  PetscBool cluster_jacobi = PETSC_FALSE;
  PetscBool cluster_tridiagonal = PETSC_FALSE;
  PetscBool multigrid = PETSC_FALSE;
  PetscBool explicit_schur = PETSC_FALSE;
  PetscBool normalize = PETSC_FALSE;
  PetscReal eta = 0.1;

  // parse options
  ierr = PetscOptionsBegin(PETSC_COMM_WORLD, NULL, "Ceres-Tao options", NULL);CHKERRQ(ierr);
  ierr = PetscOptionsString("-bal", "BAL problem", "", filename, filename,
                            PETSC_MAX_PATH_LEN, &bal);CHKERRQ(ierr);
  ierr =
      PetscOptionsString("-bal_out", "Output final solution as BAL problem", "",
                         bal_out, bal_out, PETSC_MAX_PATH_LEN, &write_bal_out);CHKERRQ(ierr);
  ierr = PetscOptionsString(
      "-bal_initial", "Output of initial solution as BAL problem", "",
      bal_initial, bal_initial, PETSC_MAX_PATH_LEN, &write_bal_initial);CHKERRQ(ierr);
  ierr = PetscOptionsString("-initial", "Initial solution to BAL problem", "",
                            initial_solution, initial_solution,
                            PETSC_MAX_PATH_LEN, &initial_solution_flag);CHKERRQ(ierr);
  ierr = PetscOptionsString("-csv", "Write out solve stats to CSV", "",
                            csv, csv,
                            PETSC_MAX_PATH_LEN, &csv_flag);CHKERRQ(ierr);
  ierr = PetscOptionsBool("-robust", "Use a robust lost function", "",
                          robust, &robust, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsBool("-use_ceres", "Use ceres to perform the solve", "",
                          use_ceres, &use_ceres, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsBool("-use_ceres_direct", "Use direct solver in Ceres", "",
                          use_ceres_direct, &use_ceres_direct, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsBool("-cluster_jacobi", "Use Ceres' cluster jacobi preconditioner", "",
                          cluster_jacobi, &cluster_jacobi, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsBool("-cluster_tridiagonal", "Use Ceres' cluster tridiagonal preconditioner", "",
                          cluster_tridiagonal, &cluster_tridiagonal, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsBool("-multigrid", "Use multigrid preconditioner", "",
                          multigrid, &multigrid, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsBool("-explicit_schur", "Use explicit schur complement with Ceres", "",
                          explicit_schur, &explicit_schur, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-rot_sigma", "Sigma of rotation perturbation", "",
                          rot_sigma, &rot_sigma, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-trans_sigma", "Sigma of translation perturbation",
                          "", trans_sigma, &trans_sigma, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-point_sigma",
                          "Sigma of point translation perturbation", "",
                          point_sigma, &point_sigma, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-eta",
                          "Eta for Nash-Sofer termination", "",
                          eta, &eta, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsBool("-verify",
                          "Verify cost, gradient, and jacobian using Ceres", "",
                          verify, &verify, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsBool("-normalize",
                          "Normalize the BAL problem before running bundle adjustment", "",
                          normalize, &normalize, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsEnd();CHKERRQ(ierr);

  if (!bal) {
    PetscPrintf(MPI_COMM_WORLD, "You must supply a bal problem");
    return 1;
  }

  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  // setup ceres problem
  // TODO: logstage this
  std::string file(filename);
  if(access(file.c_str(), F_OK) == -1) {
    std::cerr << "File " << file << " does not exist" << std::endl;
    std::exit(1);
  }
  PetscPrintf(MPI_COMM_WORLD, "Reading BAL problem '%s'.\n", file.c_str());
  BALProblem bal_problem(file, false);
  PetscPrintf(MPI_COMM_WORLD,
              "BAL Problem has %d cameras, %d points, and %d observations.\n",
              bal_problem.num_cameras(), bal_problem.num_points(),
              bal_problem.num_observations());
  if(normalize) {
    bal_problem.Normalize();
  }

  // Load initial solution
  // Vector order is defined by the order in the BAL problem data structure.
  if (initial_solution_flag) {
    // Load petsc vector
    PetscViewer initial_viewer;
    ierr = PetscViewerCreate(MPI_COMM_WORLD, &initial_viewer);CHKERRQ(ierr);
    ierr = PetscViewerFileSetName(initial_viewer, initial_solution);CHKERRQ(ierr);
    Vec initial_solution_vec;
    ierr = VecCreate(MPI_COMM_WORLD, &initial_solution_vec);CHKERRQ(ierr);
    ierr = VecLoad(initial_solution_vec, initial_viewer);CHKERRQ(ierr);

    // Have to allgather the vector so each processor can update the BAL problem
    VecScatter scatter;
    Vec local_initial_solution;
    ierr = VecScatterCreateToAll(initial_solution_vec, &scatter,
                                 &local_initial_solution);CHKERRQ(ierr);
    ierr =
        VecScatterBegin(scatter, initial_solution_vec, local_initial_solution,
                        INSERT_VALUES, SCATTER_FORWARD);CHKERRQ(ierr);
    ierr = VecScatterEnd(scatter, initial_solution_vec, local_initial_solution,
                         INSERT_VALUES, SCATTER_FORWARD);CHKERRQ(ierr);

    // Copy values over
    PetscScalar* ary;
    ierr = VecGetArray(local_initial_solution, &ary);CHKERRQ(ierr);
    std::copy_n(ary, bal_problem.num_parameters(),
                bal_problem.mutable_parameters());

    ierr = VecDestroy(&local_initial_solution);CHKERRQ(ierr);
    ierr = VecDestroy(&initial_solution_vec);CHKERRQ(ierr);
    ierr = PetscViewerDestroy(&initial_viewer);CHKERRQ(ierr);
    ierr = VecScatterDestroy(&scatter);CHKERRQ(ierr);
  }

  // bal_problem.Perturb(rot_sigma, trans_sigma, point_sigma);
  if (write_bal_initial && rank == 0) {
    bal_problem.WriteToFile(bal_initial);
  }

  if (use_ceres) {
    if (rank == 0) {
      PetscInt num_iter = 50;
      ierr = PetscOptionsGetInt(NULL, NULL, "-tao_max_it", &num_iter, NULL);CHKERRQ(ierr);
      std::unique_ptr<ceres::IterationCallback> csv_callback;

      ceres::Problem problem;
      auto ordering = std::make_shared<ceres::ParameterBlockOrdering>();
      BuildProblem(bal_problem, &problem, ordering, robust);
      Solver::Options options;
      options.linear_solver_ordering = ordering;
      options.minimizer_type = ceres::TRUST_REGION;
      options.trust_region_strategy_type = ceres::LEVENBERG_MARQUARDT;
      options.minimizer_progress_to_stdout = true;
      options.gradient_tolerance = 1e-16;
      options.function_tolerance = 1e-16;
      options.max_num_iterations = num_iter;
      options.jacobi_scaling = true;
      options.max_linear_solver_iterations = 100000;
      options.num_threads = 1;
      options.use_explicit_schur_complement = explicit_schur;
      // options.trust_region_minimizer_iterations_to_dump = {0,1};
      // options.trust_region_problem_dump_directory = "/Users/tristan/Sync/Research/bundle_adjustment/ba-tao/dumps";
      std::string solver = "none";
      if(use_ceres_direct) {
        options.linear_solver_type = SPARSE_SCHUR;
        solver = "cholmod";
      } else {
        options.linear_solver_type = ITERATIVE_SCHUR;
        if(cluster_jacobi) {
          options.preconditioner_type = CLUSTER_JACOBI;
          solver = "visibility";
        } else if(cluster_tridiagonal) {
          options.preconditioner_type = CLUSTER_TRIDIAGONAL;
          solver = "visibility_tridiagonal";
        } else if(multigrid) {
          solver = "multigrid";
          options.preconditioner_type = JULIA_MULTIGRID;
        } else {
          if(explicit_schur) {
            solver = "pbjacobi_explicit";
          } else {
            solver = "pbjacobi_implicit";
          }
          options.preconditioner_type = SCHUR_JACOBI;
        }
      }
      if(csv_flag) {
        csv_callback.reset(new CSVCallback(std::string(csv), solver, bal_problem, robust ? "huber" : "none"));
        options.callbacks = {csv_callback.get()};
      }
      options.eta = eta;
      options.sparse_linear_algebra_library_type = SUITE_SPARSE;
      Solver::Summary summary;
      std::cout << "Solving problem" << std::endl;
      Solve(options, &problem, &summary);
      std::cout << summary.FullReport() << "\n";
    }
    MPI_Barrier(MPI_COMM_WORLD);
  } else {
    PetscLogStage setup_stage, solve_stage;
    ierr = PetscLogStageRegister("Setup", &setup_stage);CHKERRQ(ierr);
    ierr = PetscLogStageRegister("Solve", &solve_stage);CHKERRQ(ierr);

    ierr = PetscPrintf(MPI_COMM_WORLD, "Setting up TAO.\n");CHKERRQ(ierr);
    ierr = PetscLogStagePush(setup_stage);CHKERRQ(ierr);

    nonstd::optional<BALProblem> verify_bal_problem;
    std::unique_ptr<ceres::Problem> verify_ceres_problem;
    if(verify) {
      verify_bal_problem = bal_problem.clone();
      verify_ceres_problem = std::unique_ptr<ceres::Problem>(new ceres::Problem());
      auto ordering = std::make_shared<ceres::ParameterBlockOrdering>();
      BuildProblem(verify_bal_problem.value(), verify_ceres_problem.get(), ordering, robust);
    }

    std::unique_ptr<DistributedProblem> dist_problem;
    ierr = DistributedProblem::create(MPI_COMM_WORLD, std::move(bal_problem),
                                      robust, dist_problem);CHKERRQ(ierr);

    Tao tao;
    ierr = create_tao(dist_problem.get(), &tao, std::move(verify_bal_problem), std::move(verify_ceres_problem));CHKERRQ(ierr);
    // Allocates space for history so we can get it for printing to csv
    ierr = TaoSetConvergenceHistory(tao,NULL,NULL,NULL,NULL,PETSC_DECIDE,PETSC_TRUE);CHKERRQ(ierr);

    if(csv_flag) {
      // we must enable logging so that we get timing events
      PetscLogDefaultBegin();
      CSVPrinter* printer = new CSVPrinter(std::string(csv), bal_problem, robust ? "huber" : "none", rank);
      TaoSetMonitor(tao, csv_monitor, printer, csv_monitor_destroy);
    }

    ierr = PetscLogStagePop();CHKERRQ(ierr);
    ierr = PetscBarrier((PetscObject)tao);CHKERRQ(ierr);

    ierr = PetscLogStagePush(solve_stage);CHKERRQ(ierr);

    ierr = PetscPrintf(MPI_COMM_WORLD, "Solving.\n");CHKERRQ(ierr);
    ierr = TaoSolve(tao);CHKERRQ(ierr);

    ierr = PetscLogStagePop();CHKERRQ(ierr);
    ierr = PetscBarrier((PetscObject)tao);CHKERRQ(ierr);


    dist_problem->copy_to_bal(tao, bal_problem);

    ierr = TaoDestroy(&tao);CHKERRQ(ierr);
  }

  if (write_bal_out && rank == 0) {
    bal_problem.WriteToFile(bal_out);
  }

  ierr = PetscFinalize();
  return ierr;
}
