#pragma once

#include <ceres/ceres.h>
#include <petsc.h>
#include <petsc/private/matimpl.h>
#include <petsc/private/taoimpl.h>
#include "bal_problem.h"
#include "optional.hpp"
#include "snavely_reprojection_error.h"
#include <array>
#include "common.hpp"
#include <Eigen/Dense>

// Code to evaluate a Ceres Problem into a PETSc Jacobian
//
// The Ceres Problem contains parameter and residual blocks. Parameter blocks
// may or may not be connected to a number of residual blocks. Each residual
// block corresponds to a number of rows and columns in the jacobian.
//
// TODO: all the orderings here are very confusing. The abstractions are not
// well specified or used.


using ceres::examples::BALProblem;

PetscScalar csc(PetscScalar x) {
  return 1.0/sin(x);
}

// Linearized rotation or R around axis
void linearized_rotation(const PetscScalar* R, const std::array<PetscScalar, 3> axis, PetscScalar* out) {
  double squared_norm = R[0] * R[0] + R[1] * R[1] + R[2] * R[2];
  if (squared_norm < PETSC_SQRT_MACHINE_EPSILON) {
    out[0] = axis[0];
    out[1] = axis[1];
    out[2] = axis[2];
  } else {
    Eigen::Map<const Eigen::Matrix<PetscScalar, 3, 1>> RR(R);
    PetscScalar alpha = RR.norm();
    Eigen::Matrix<PetscScalar, 3, 1> r = RR / alpha;
    Eigen::Map<const Eigen::Matrix<PetscScalar, 3, 1>> a(axis.data());

    Eigen::Map<Eigen::Matrix<PetscScalar, 3, 1>> o(out);
    o = 0.5 * abs(csc(alpha/2))*(r*(-alpha*cos(alpha/2)+sqrt(2-2*cos(alpha)))*r.dot(-a)+alpha*(-a*cos(alpha/2)+r.cross(-a)*sin(alpha/2)));
  }
}

PetscErrorCode DMPlexDistributeIS(DM dm, PetscSF sf,
                                  PetscSection original_section, IS orig_is,
                                  PetscSection new_section, IS *new_is) {
  PetscErrorCode ierr;
  PetscFunctionBegin;

  const PetscInt *indices;
  PetscInt *new_indices;
  ierr = ISGetIndices(orig_is, &indices);CHKERRQ(ierr);
  ierr =
      DMPlexDistributeData(dm, sf, original_section, MPIU_INT, (void *)indices,
                           new_section, (void **)&new_indices);CHKERRQ(ierr);
  ierr = ISRestoreIndices(orig_is, &indices);CHKERRQ(ierr);
  PetscInt n;
  ierr = PetscSectionGetStorageSize(new_section, &n);CHKERRQ(ierr);
  ierr = ISCreateGeneral(PetscObjectComm((PetscObject)dm), n, new_indices,
                         PETSC_OWN_POINTER, new_is);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

// XXX: TODO: Apparently DMLabelGetStratumIS is broken and only works locally.
PETSC_STATIC_INLINE PetscErrorCode DMLabelGetStratumISOnComm_Private(DMLabel label, PetscInt value, MPI_Comm comm, IS *is)
{
  IS                tmpis;
  PetscErrorCode    ierr;

  PetscFunctionBegin;
  ierr = DMLabelGetStratumIS(label, value, &tmpis);CHKERRQ(ierr);
  if (!tmpis) {ierr = ISCreateGeneral(PETSC_COMM_SELF, 0, NULL, PETSC_USE_POINTER, &tmpis);CHKERRQ(ierr);}
  ierr = ISOnComm(tmpis, comm, PETSC_COPY_VALUES, is);CHKERRQ(ierr);
  ierr = ISDestroy(&tmpis);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode MatAIJSetPreallocationCSR(Mat B, const PetscInt i[],
                                         const PetscInt j[],
                                         const PetscScalar v[]) {
  PetscErrorCode ierr;
  PetscFunctionBegin;

  MatType type;
  ierr = MatGetType(B, &type);CHKERRQ(ierr);
  if (std::string(type) == MATSEQAIJ) {
    ierr = MatSeqAIJSetPreallocationCSR(B, i, j, v);CHKERRQ(ierr);
  } else {
    ierr = MatMPIAIJSetPreallocationCSR(B, i, j, v);CHKERRQ(ierr);
  }

  PetscFunctionReturn(0);
}

PetscErrorCode GetCameraPointOffsets(DM dm, PetscInt *camera_offset,
                                     PetscInt *point_offset) {
  PetscErrorCode ierr;
  PetscFunctionBegin;

  // Fill global array with offsets of each element
  PetscInt n, n_local, vec_off;
  Vec global_vec, local_vec;
  ierr = DMGetGlobalVector(dm, &global_vec);CHKERRQ(ierr);
  ierr = VecGetLocalSize(global_vec, &n);CHKERRQ(ierr);
  ierr = VecGetOwnershipRange(global_vec, &vec_off, NULL);CHKERRQ(ierr);
  std::vector<PetscInt> global_offsets(n);
  std::iota(global_offsets.begin(), global_offsets.end(), vec_off);

  ierr = DMGetLocalVector(dm, &local_vec);CHKERRQ(ierr);
  ierr = VecGetLocalSize(local_vec, &n_local);CHKERRQ(ierr);
  std::vector<PetscInt> local_offsets(n_local);

  // Global to local. Modified from dm.c:DMGlobalToLocalBegin to use MPIU_INT.
  PetscSF sf;
  ierr = DMGetSectionSF(dm, &sf);CHKERRQ(ierr);
  ierr = PetscSFBcastBegin(sf, MPIU_INT, global_offsets.data(),
                           local_offsets.data());CHKERRQ(ierr);
  ierr = PetscSFBcastEnd(sf, MPIU_INT, global_offsets.data(),
                         local_offsets.data());CHKERRQ(ierr);

  // Iterate over all points to find the smallest offset
  MPI_Comm comm = PetscObjectComm((PetscObject)dm);
  PetscSection local_section;
  ierr = DMGetCoordinateSection(dm, &local_section);CHKERRQ(ierr);
  PetscInt min_camera = INT_MAX;
  PetscInt cameras_start, cameras_end;
  ierr = DMPlexGetDepthStratum(dm, 1, &cameras_start, &cameras_end);CHKERRQ(ierr);
  for (PetscInt i = cameras_start; i < cameras_end; i++) {
    PetscInt off;
    ierr = PetscSectionGetOffset(local_section, i, &off);CHKERRQ(ierr);
    min_camera = std::min(local_offsets[off], min_camera);
  }
  PetscInt camera_global_start;
  MPI_Allreduce(&min_camera, camera_offset, 1, MPIU_INT, MPI_MIN, comm);

  PetscInt min_point = INT_MAX;
  PetscInt points_start, points_end;
  ierr = DMPlexGetDepthStratum(dm, 0, &points_start, &points_end);CHKERRQ(ierr);
  for (PetscInt i = points_start; i < points_end; i++) {
    PetscInt off;
    ierr = PetscSectionGetOffset(local_section, i, &off);CHKERRQ(ierr);
    min_point = std::min(local_offsets[off], min_point);
  }
  MPI_Allreduce(&min_point, point_offset, 1, MPIU_INT, MPI_MIN, comm);

  PetscFunctionReturn(0);
}

// TODO: this should really create a PetscSection for cameras and one for points
PetscErrorCode CreateCameraPointSubsections(
    DM dm, std::vector<PetscInt> &local_indices) {
  PetscErrorCode ierr;
  PetscSection global_section;
  PetscSF sf;
  DMLabel depth_label;
  PetscFunctionBegin;

  MPI_Comm comm = PetscObjectComm((PetscObject)dm);

  ierr = DMGetCoordinateSection(dm, &global_section);CHKERRQ(ierr);

  // Count local number of points and cameras
  PetscInt num_local_cameras = 0;
  std::vector<PetscInt> camera_local_indices;

  PetscInt camera_start, camera_end;
  ierr = DMPlexGetDepthStratum(dm, 1, &camera_start, &camera_end);CHKERRQ(ierr);
  for (PetscInt i = camera_start; i < camera_end; i++) {
    PetscInt off;
    ierr = DMPlexGetPointGlobal(dm, i, &off, NULL);CHKERRQ(ierr);
    if (off >= 0) {
      num_local_cameras++;
      camera_local_indices.push_back(i);
    }
  }

  PetscInt camera_index_offset;
  MPI_Scan(&num_local_cameras, &camera_index_offset, 1, MPIU_INT, MPI_SUM,
           comm);
  camera_index_offset -= num_local_cameras;

  PetscInt num_local_points = 0;
  std::vector<PetscInt> point_local_indices;

  PetscInt point_start, point_end;
  ierr = DMPlexGetDepthStratum(dm, 0, &point_start, &point_end);CHKERRQ(ierr);
  for (PetscInt i = point_start; i < point_end; i++) {
    PetscInt off;
    ierr = DMPlexGetPointGlobal(dm, i, &off, NULL);CHKERRQ(ierr);
    if (off >= 0) {
      num_local_points++;
      point_local_indices.push_back(i);
    }
  }

  PetscInt point_index_offset;
  MPI_Scan(&num_local_points, &point_index_offset, 1, MPIU_INT, MPI_SUM, comm);
  point_index_offset -= num_local_points;

  PetscInt camera_dof, point_dof;
  ierr =
      PetscSectionGetDof(global_section, camera_local_indices[0], &camera_dof);CHKERRQ(ierr);
  ierr = PetscSectionGetDof(global_section, point_local_indices[0], &point_dof);CHKERRQ(ierr);

  // label local points
  // order is cameras then points
  std::vector<PetscInt> global_indices(num_local_points * point_dof + num_local_cameras * camera_dof);
  for (PetscInt i = 0; i < camera_local_indices.size(); i++) {
    for(PetscInt j = 0; j < camera_dof; j++) {
      global_indices[i * camera_dof + j] =
          (camera_index_offset + i) *
          camera_dof;
    }
  }
  for (PetscInt i = 0; i < point_local_indices.size(); i++) {
    for(PetscInt j = 0; j < point_dof; j++) {
      global_indices[camera_local_indices.size() * camera_dof + i * point_dof + j] =
          (point_index_offset + i) * point_dof;
    }
  }

  // use global to local to send global indices to the correct positions
  Vec local_vec;
  PetscInt n;
  ierr = DMGetLocalVector(dm, &local_vec);CHKERRQ(ierr);
  ierr = VecGetLocalSize(local_vec, &n);CHKERRQ(ierr);

  std::vector<PetscInt> all_local_indices(n);
  ierr = DMGetSectionSF(dm, &sf);CHKERRQ(ierr);
  int size;
  MPI_Comm_size(comm, &size);
  if(size > 1) {
    ierr = PetscSFBcastBegin(sf, MPIU_INT, global_indices.data(),
                             all_local_indices.data());CHKERRQ(ierr);
    ierr = PetscSFBcastEnd(sf, MPIU_INT, global_indices.data(),
                           all_local_indices.data());CHKERRQ(ierr);
  } else {
    assert(global_indices.size() == all_local_indices.size());
    std::copy(global_indices.begin(), global_indices.end(), all_local_indices.begin());
  }

  local_indices.resize(point_end);
  for(PetscInt i = camera_start; i < camera_end; i++) {
    local_indices[i] = all_local_indices[i * camera_dof];
  }
  for(PetscInt i = point_start; i < point_end; i++) {
    local_indices[i] = all_local_indices[camera_end * camera_dof + (i - point_start) * point_dof];
  }

  PetscFunctionReturn(0);
}

struct Cost {
  PetscInt camera;  // local index into parameters array
  PetscInt point;   // local index into parameters array
  std::unique_ptr<ceres::CostFunction> f;
  PetscReal observed_x, observed_y;
  Cost(PetscInt c, PetscInt p, PetscReal observed_x, PetscReal observed_y)
      : camera(c),
        point(p),
        f(ceres::examples::SnavelyReprojectionError::Create(observed_x, observed_y)),
        observed_x(observed_x),
        observed_y(observed_y) {}
  bool Evaluate(const double *camera_params, const double *point_params,
                double *cost, double *residuals, double **jacobians) const {
    const double* params[2] = {camera_params, point_params};
    auto t = f->Evaluate(params, residuals, jacobians);

    // compute cost as residuals.norm()^2 * 0.5
    *cost = 0;
    for(int i = 0; i < num_residuals(); i++) {
      *cost += residuals[i] * residuals[i];
    }
    *cost *= 0.5;
    return t;
  }

  int num_residuals() const {
    return f->num_residuals();
  }

  int camera_block_size() const {
    return f->parameter_block_sizes()[0];
  }

  int point_block_size() const {
    return f->parameter_block_sizes()[1];
  }
};

// Create a DMPlex from a BALProblem. In order to get the right layout, cameras
// point to points. Stratum 1 is cameras, stratum 0 is points.
// `observations` contains the observations reordered to match the layout of the
// cones.
PetscErrorCode DMPlexFromBAL(MPI_Comm comm, const BALProblem &problem, DM *dm,
                             std::vector<PetscReal> &observations) {
  PetscErrorCode ierr;
  PetscFunctionBegin;

  int rank;
  MPI_Comm_rank(comm, &rank);

  ierr = DMPlexCreate(comm, dm);CHKERRQ(ierr);

  // Copied from src/dm/imples/plex/plexcreate.c:DMPlexCreate cause its
  // easier to do it manually then figure out what is happening internally.
  DMPlexSetChart(*dm, 0, problem.num_cameras() + problem.num_points());

  // Camera adjacency list
  // Camera indices go from [0..num_cameras). Point indices go from
  // [num_cameras..num_cameras+num_points).
  std::vector<std::vector<PetscInt>> cones(problem.num_cameras());
  // Mapping from camera, point pair to observation index. Used to reorder the
  // observations.
  std::map<std::pair<PetscInt, PetscInt>, PetscInt> obs_map;
  if (rank == 0) {  // construction only happens on the first processor
    for (PetscInt i = 0; i < problem.num_observations(); i++) {
      cones[problem.camera_index()[i]].push_back(problem.num_cameras() +
                                                 problem.point_index()[i]);
      obs_map[{problem.camera_index()[i],
               problem.num_cameras() + problem.point_index()[i]}] = i;
    }
    for (PetscInt i = 0; i < cones.size(); i++) {
      ierr = DMPlexSetConeSize(*dm, i, cones[i].size());CHKERRQ(ierr);
    }
    // Points are connected to nothing
    for (PetscInt i = 0; i < problem.num_points(); i++) {
      ierr = DMPlexSetConeSize(*dm, problem.num_cameras() + i, 0);CHKERRQ(ierr);
    }
  }

  // allocates space for cones
  ierr = DMSetUp(*dm);CHKERRQ(ierr);

  // set connections between cameras and points
  if (rank == 0) {
    for (PetscInt i = 0; i < cones.size(); i++) {
      ierr = DMPlexSetCone(*dm, i, cones[i].data());CHKERRQ(ierr);
    }
  }

  // this is required apparently
  ierr = DMPlexSymmetrize(*dm);CHKERRQ(ierr);
  ierr = DMPlexStratify(*dm);CHKERRQ(ierr);
  ierr = DMSetCoordinateDim(*dm, 1);CHKERRQ(ierr);

  // Set the dofs on each camera/point. We are abusing this PETSc
  // architecture, so coordinates really are parameters.
  PetscSection param_section;
  ierr = DMGetCoordinateSection(*dm, &param_section);CHKERRQ(ierr);
  ierr = PetscSectionSetNumFields(param_section, 1);CHKERRQ(ierr);
  ierr = PetscSectionSetFieldComponents(param_section, 0, 1);CHKERRQ(ierr);
  ierr = PetscSectionSetChart(param_section, 0,
                              problem.num_cameras() + problem.num_points());CHKERRQ(ierr);
  if (rank == 0) {
    for (PetscInt i = 0; i < problem.num_cameras(); i++) {
      // I don't know why or if we need both of these.
      ierr = PetscSectionSetDof(param_section, i, problem.camera_block_size());CHKERRQ(ierr);
      ierr = PetscSectionSetFieldDof(param_section, i, 0,
                                     problem.camera_block_size());CHKERRQ(ierr);
    }
    for (PetscInt i = 0; i < problem.num_points(); i++) {
      // I don't know why we need both of these.
      ierr = PetscSectionSetDof(param_section, problem.num_cameras() + i,
                                problem.point_block_size());CHKERRQ(ierr);
      ierr = PetscSectionSetFieldDof(param_section, problem.num_cameras() + i,
                                     0, problem.point_block_size());CHKERRQ(ierr);
    }
  }
  ierr = PetscSectionSetUp(param_section);CHKERRQ(ierr);
  PetscSection orig_section;
  ierr = PetscSectionClone(param_section, &orig_section);CHKERRQ(ierr);

  Vec parameters;
  ierr = VecCreate(PETSC_COMM_SELF, &parameters);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject)parameters, "parameters");CHKERRQ(ierr);
  PetscInt paramSize;
  ierr = PetscSectionGetStorageSize(param_section, &paramSize);CHKERRQ(ierr);
  ierr = VecSetSizes(parameters, paramSize, PETSC_DETERMINE);CHKERRQ(ierr);
  ierr = VecSetBlockSize(parameters, 1);CHKERRQ(ierr);
  // TODO: set from options?
  ierr = VecSetType(parameters, VECSTANDARD);CHKERRQ(ierr);
  if (rank == 0) {
    PetscReal *params;
    ierr = VecGetArray(parameters, &params);CHKERRQ(ierr);
    for (PetscInt i = 0; i < problem.num_cameras(); i++) {
      PetscInt off;
      ierr = PetscSectionGetOffset(param_section, i, &off);CHKERRQ(ierr);
      std::copy_n(problem.cameras() + i * problem.camera_block_size(),
                  problem.camera_block_size(), params + off);
    }
    for (PetscInt i = 0; i < problem.num_points(); i++) {
      PetscInt off;
      ierr =
          PetscSectionGetOffset(param_section, problem.num_cameras() + i, &off);CHKERRQ(ierr);
      std::copy_n(problem.points() + i * problem.point_block_size(),
                  problem.point_block_size(), params + off);
    }
    ierr = VecRestoreArray(parameters, &params);CHKERRQ(ierr);
  }
  // TODO: are both of these needed?
  ierr = DMSetCoordinateSection(*dm, 1, param_section);CHKERRQ(ierr);
  // _MUST_ set this for GlobalToLocal and friends.
  ierr = DMSetSection(*dm, param_section);CHKERRQ(ierr);

  ierr = DMSetCoordinatesLocal(*dm, parameters);CHKERRQ(ierr);

  // Create new observations ordering
  if (rank == 0) {
    observations.reserve(problem.num_observations() * 2);
    PetscSection cone_section;
    ierr = DMPlexGetConeSection(*dm, &cone_section);CHKERRQ(ierr);
    for (PetscInt i = 0; i < problem.num_cameras(); i++) {
      PetscInt cone_size;
      ierr = PetscSectionGetDof(cone_section, i, &cone_size);CHKERRQ(ierr);
      const PetscInt *cone;
      ierr = DMPlexGetCone(*dm, i, &cone);CHKERRQ(ierr);
      for (PetscInt j = 0; j < cone_size; j++) {
        auto obs_i = obs_map[{i, cone[j]}];
        observations.push_back(problem.observations()[obs_i * 2]);
        observations.push_back(problem.observations()[obs_i * 2 + 1]);
      }
    }
  }

  // This is a hack as https://bitbucket.org/petsc/petsc/pull-requests/1221/knepley-fix-plex-partition-parallel broke construction
  int depth;
  ierr = DMPlexGetDepth(*dm, &depth);CHKERRQ(ierr);
  ierr = DMSetDimension(*dm, depth);CHKERRQ(ierr);

  ierr = VecDestroy(&parameters);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

// Mapping from index in local section to offset in global camera vector or
// global point vector.
PetscErrorCode create_camera_points_global_offsets(
    PetscSection param_section, PetscInt camera_block_size,
    std::vector<PetscInt> &offsets) {
  PetscErrorCode ierr;
  PetscFunctionBegin;

  PetscInt p_start, p_end;
  ierr = PetscSectionGetChart(param_section, &p_start, &p_end);CHKERRQ(ierr);
  if (p_start != 0) {
    SETERRQ(PetscObjectComm((PetscObject)param_section), PETSC_ERR_LIB,
            "local section must start at 0");
  }

  PetscInt camera_off = 0;
  PetscInt point_off = 0;
  for (PetscInt i = p_start; i < p_end; i++) {
    PetscInt dof, off;
    // should use DMGetStratum
    ierr = PetscSectionGetDof(param_section, i, &dof);CHKERRQ(ierr);
    ierr = PetscSectionGetOffset(param_section, i, &off);CHKERRQ(ierr);
    if (off < 0) continue;
    if (dof == camera_block_size) {  // TODO: there should be another vector
                                     // holding the type of each of these.
                                     // Could use a label?
      offsets.push_back(camera_off);
      camera_off += dof;
    } else {
      offsets.push_back(point_off);
      point_off += dof;
    }
  }

  PetscFunctionReturn(0);
}

PetscErrorCode create_costs(
    DM dm, const std::vector<double> &observations, bool robust,
    std::vector<Cost> &costs) {
  PetscErrorCode ierr;
  PetscFunctionBegin;

  // Get local edges and create cost functions
  PetscSection cone_section, param_section;
  ierr = DMPlexGetConeSection(dm, &cone_section);CHKERRQ(ierr);
  ierr = DMGetCoordinateSection(dm, &param_section);CHKERRQ(ierr);

  PetscInt cones_start, cones_end;
  ierr = PetscSectionGetChart(cone_section, &cones_start, &cones_end);CHKERRQ(ierr);

  PetscInt cameras_start, cameras_end;
  // Stratum 1 is cameras, stratum 0 is points
  ierr = DMPlexGetDepthStratum(dm, 1, &cameras_start, &cameras_end);CHKERRQ(ierr);

  PetscSection local_param_section;
  ierr = DMGetCoordinateSection(dm, &local_param_section);CHKERRQ(ierr);

  // Create parameter blocks
  // Layout is cameras then points. Can be indexed using points from the local
  // parameter section or cone section or from points in the cone (they are the
  // same).
  Vec parameters;
  ierr = DMGetCoordinatesLocal(dm, &parameters);CHKERRQ(ierr);
  double *local_parameters;
  ierr = VecGetArray(parameters, &local_parameters);CHKERRQ(ierr);
  for (PetscInt p = cones_start; p < cones_end; p++) {
    PetscInt dof, off;
    ierr = PetscSectionGetDof(local_param_section, p, &dof);CHKERRQ(ierr);
    ierr = PetscSectionGetOffset(local_param_section, p, &off);CHKERRQ(ierr);
  }
  ierr = VecRestoreArray(parameters, &local_parameters);CHKERRQ(ierr);

  // Create residual blocks
  // Cones index from cameras to points.
  PetscInt *local_cones;
  ierr = DMPlexGetCones(dm, &local_cones);CHKERRQ(ierr);
  for (PetscInt c = cameras_start; c < cameras_end; c++) {
    PetscInt num_obs;
    PetscInt obs_offset;
    ierr = PetscSectionGetDof(cone_section, c, &num_obs);CHKERRQ(ierr);
    ierr = PetscSectionGetOffset(cone_section, c, &obs_offset);CHKERRQ(ierr);

    for (PetscInt o = 0; o < num_obs; o++) {
      PetscInt p = local_cones[obs_offset + o];
      assert(local_cones[obs_offset + o] < cones_end);
      auto obs_x = observations[(obs_offset + o) * 2];
      auto obs_y = observations[(obs_offset + o) * 2 + 1];
      costs.emplace_back(c, p, obs_x, obs_y);
    }
  }

  PetscFunctionReturn(0);
}

struct DistributedProblem {
  static PetscErrorCode create(MPI_Comm comm, BALProblem &&problem, bool robust,
                               std::unique_ptr<DistributedProblem> &dp) {
    PetscErrorCode ierr;
    PetscFunctionBegin;

    int rank;
    MPI_Comm_rank(comm, &rank);
    int comm_size;
    MPI_Comm_size(comm, &comm_size);

    // Create DMPlex to hold parameters
    DM dm;
    std::vector<double> observations;
    ierr = DMPlexFromBAL(comm, problem, &dm, observations);CHKERRQ(ierr);

    // To save the original ordering
    ierr = DMSetUseNatural(dm, PETSC_TRUE);CHKERRQ(ierr);

    // Used to create a global to natural mapping for resiudals (for mapping
    // gradient and jacobian back to ceres ordering).
    std::vector<PetscInt> residual_ids(problem.num_observations() * 2);
    std::iota(residual_ids.begin(), residual_ids.end(), 0);

    // Distribute our DM
    DM dm_dist;
    PetscSF sf;
    std::vector<double> dist_observations;
    if (comm_size > 1) {
      PetscPartitioner part;
      ierr = DMPlexGetPartitioner(dm, &part);CHKERRQ(ierr);
      ierr = PetscPartitionerSetFromOptions(part);CHKERRQ(ierr);
      ierr = DMPlexDistribute(dm, 0, &sf, &dm_dist);CHKERRQ(ierr);

      // Distribute observations data. The data has the same layout as the cone
      // section.
      PetscSection original_cone_section, dist_cone_section;
      ierr = DMPlexGetConeSection(dm, &original_cone_section);CHKERRQ(ierr);
      ierr = DMPlexGetConeSection(dm_dist, &dist_cone_section);CHKERRQ(ierr);
      double *dist_obs;
      MPI_Datatype double_pair;
      MPI_Type_contiguous(2, MPIU_REAL, &double_pair);
      MPI_Type_commit(&double_pair);
      ierr = DMPlexDistributeData(dm_dist, sf, original_cone_section,
                                  double_pair, (void *)observations.data(),
                                  dist_cone_section, (void **)&dist_obs);CHKERRQ(ierr);
      PetscInt n;
      ierr = PetscSectionGetStorageSize(dist_cone_section, &n);CHKERRQ(ierr);
      dist_observations.resize(2 * n);
      std::copy_n(dist_obs, 2 * n, dist_observations.begin());

      // Distribute residual ids
      PetscInt *dist_residual_ids;
      MPI_Datatype int_pair;
      MPI_Type_contiguous(2, MPIU_INT, &int_pair);
      MPI_Type_commit(&int_pair);
      ierr =
          DMPlexDistributeData(dm_dist, sf, original_cone_section, int_pair,
                               (void *)residual_ids.data(), dist_cone_section,
                               (void **)&dist_residual_ids);CHKERRQ(ierr);
      residual_ids.resize(2 * n);
      std::copy_n(dist_residual_ids, 2 * n, residual_ids.begin());

      ierr = PetscFree(dist_obs);CHKERRQ(ierr);
      ierr = PetscFree(dist_residual_ids);CHKERRQ(ierr);
      ierr = DMDestroy(&dm);CHKERRQ(ierr);
    } else {
      PetscLayout map;
      dm_dist = dm;
      dm = NULL;

      Vec v;
      ierr = DMGetGlobalVector(dm_dist, &v);CHKERRQ(ierr);
      ierr = VecGetLayout(v,&map);CHKERRQ(ierr);
      PetscSF sf;
      ierr = PetscSFCreate(comm, &sf);CHKERRQ(ierr);
      ierr = PetscSFSetGraphWithPattern(sf, map, PETSCSF_PATTERN_GATHER);CHKERRQ(ierr);
      ierr = DMPlexSetGlobalToNaturalSF(dm_dist, sf);CHKERRQ(ierr);

      dist_observations = observations;
    }

    IS residual_global_to_natural;
    ierr = ISCreateGeneral(comm, residual_ids.size(), residual_ids.data(),
                           PETSC_COPY_VALUES, &residual_global_to_natural);CHKERRQ(ierr);
    ierr = ISSetPermutation(residual_global_to_natural);CHKERRQ(ierr);

    std::vector<Cost> costs;
    ierr =
        create_costs(dm_dist, dist_observations, robust, costs);CHKERRQ(ierr);

    // Distribute IS so that we have a local to global mapping
    ISLocalToGlobalMapping is_l2g;
    ierr = DMGetLocalToGlobalMapping(dm_dist, &is_l2g);CHKERRQ(ierr);

    Vec residuals_vec;
    ierr = VecCreateMPI(
        comm, costs.size() * costs.at(0).num_residuals(),
        PETSC_DETERMINE, &residuals_vec);CHKERRQ(ierr);

    // Find the start of cameras and point by iterating over each local index.
    PetscInt camera_global_start, point_global_start;
    ierr = GetCameraPointOffsets(dm_dist, &camera_global_start,
                                 &point_global_start);

    // TODO: can I get rid of this?
    std::vector<PetscInt> local_indices;
    ierr = CreateCameraPointSubsections(dm_dist, local_indices);CHKERRQ(ierr);

    // Get IS for cameras and points
    DMLabel depth_label;
    ierr = DMPlexGetDepthLabel(dm_dist, &depth_label);CHKERRQ(ierr);
    IS camera_is, point_is;
    ierr = DMLabelGetStratumISOnComm_Private(depth_label, 0, comm, &point_is);CHKERRQ(ierr);
    ierr = DMLabelGetStratumISOnComm_Private(depth_label, 1, comm, &camera_is);CHKERRQ(ierr);

    dp = std::unique_ptr<DistributedProblem>(new DistributedProblem{
        comm, dm_dist, is_l2g, std::move(costs),
        residuals_vec, residual_global_to_natural, camera_is, point_is, local_indices,
        comm_size <= 1});

    // TODO: cleanup
    PetscFunctionReturn(0);
  }

  PetscErrorCode get_parameter_vec(Vec *p) const {
    PetscErrorCode ierr;
    PetscFunctionBegin;

    ierr = DMGetCoordinates(dm, p);CHKERRQ(ierr);

    PetscFunctionReturn(0);
  }

  PetscErrorCode create_gradient_vec(Vec *grad) const {
    PetscErrorCode ierr;
    PetscFunctionBegin;

    PetscSection section;
    ierr = DMGetCoordinateSection(dm, &section);CHKERRQ(ierr);
    PetscLayout layout;
    ierr = PetscSectionGetValueLayout(comm, section, &layout);CHKERRQ(ierr);
    PetscInt n, N;
    ierr = PetscLayoutGetLocalSize(layout, &n);CHKERRQ(ierr);
    ierr = PetscLayoutGetSize(layout, &N);CHKERRQ(ierr);

    ierr = VecCreateMPI(comm, n, N, grad);CHKERRQ(ierr);

    PetscFunctionReturn(0);
  }

  PetscErrorCode create_jacobian(Mat *J, MatType mattype = MATSAME) const {
    PetscErrorCode ierr;
    PetscFunctionBegin;

    ierr = MatCreate(comm, J);CHKERRQ(ierr);

    // Get global layout.
    PetscSection section;
    Vec params;
    ierr = DMGetCoordinates(dm, &params);CHKERRQ(ierr);
    PetscInt n, N, m, M;
    ierr = VecGetSize(params, &N);CHKERRQ(ierr);
    ierr = VecGetLocalSize(params, &n);CHKERRQ(ierr);

    ierr = VecGetSize(residuals_vec, &M);CHKERRQ(ierr);
    ierr = VecGetLocalSize(residuals_vec, &m);CHKERRQ(ierr);

    // Set matrix size.
    ierr = MatSetSizes(*J, m, n, M, N);CHKERRQ(ierr);

    ierr = MatSetOptionsPrefix(*J, "ba_jacobian_");CHKERRQ(ierr);
    ierr = MatSetBlockSizes(*J, 2, point_block_size());CHKERRQ(ierr);
    ierr = MatSetFromOptions(*J);CHKERRQ(ierr);
    // We must use MATNEST so that the block sizes are propogated correctly
    ierr = MatSetType(*J, MATNEST);CHKERRQ(ierr);
    if (std::string(mattype) != MATSAME) {
      ierr = MatSetType(*J, mattype);CHKERRQ(ierr);
    }

    MatType mat_type;
    ierr = MatGetType(*J, &mat_type);CHKERRQ(ierr);

    bool is_nest = std::string(mat_type) == MATNEST;

    // Set preallocation.
    PetscSection local_section;
    ierr = DMGetCoordinateSection(dm, &local_section);CHKERRQ(ierr);
    std::vector<PetscInt> row_ptr;
    std::vector<PetscInt> row_ptr_camera;
    std::vector<PetscInt> row_ptr_point;
    if (is_nest) {
      row_ptr_camera.reserve(num_local_residuals() + 1);
      row_ptr_camera.push_back(0);
      row_ptr_point.reserve(num_local_residuals() + 1);
      row_ptr_point.push_back(0);
    } else {
      row_ptr.reserve(num_local_residuals() + 1);
      row_ptr.push_back(0);
    }
    std::vector<PetscInt> cols;
    std::vector<PetscInt> cols_camera;
    std::vector<PetscInt> cols_point;
    if (is_nest) {
      cols_camera.reserve(num_local_residuals() * camera_block_size());
      cols_point.reserve(num_local_residuals() * point_block_size());
    } else {
      cols.reserve(num_local_residuals() *
                   (camera_block_size() + point_block_size()));
    }
    PetscInt ps = parameter_size();
    for (PetscInt i = 0; i < num_local_residual_blocks(); i++) {
      for (PetscInt block_i = 0; block_i < residual_block_size(); block_i++) {
        if (is_nest) {
          row_ptr_camera.push_back((i * residual_block_size() + block_i + 1) *
                                   camera_block_size());
          row_ptr_point.push_back((i * residual_block_size() + block_i + 1) *
                                  point_block_size());
        } else {
          row_ptr.push_back((i * residual_block_size() + block_i + 1) * ps);
        }

        // add camera indices
        PetscInt off, dof;
        ierr = PetscSectionGetOffset(local_section, costs[i].camera, &off);CHKERRQ(ierr);
        ierr = PetscSectionGetDof(local_section, costs[i].camera, &dof);CHKERRQ(ierr);
        for (PetscInt j = 0; j < dof; j++) {
          if (is_nest) {
            cols_camera.push_back(
                camera_or_point_global_offset(costs[i].camera) + j);
          } else {
            cols.push_back(off + j);
          }
        }

        // add point indices
        ierr = PetscSectionGetOffset(local_section, costs[i].point, &off);CHKERRQ(ierr);
        ierr = PetscSectionGetDof(local_section, costs[i].point, &dof);CHKERRQ(ierr);
        for (PetscInt j = 0; j < dof; j++) {
          if (is_nest) {
            cols_point.push_back(camera_or_point_global_offset(costs[i].point) +
                                 j);
          } else {
            cols.push_back(off + j);
          }
        }
      }
    }
    if (is_nest) {
      // Ordering is cameras then points as this is what fieldsplit wants
      IS iss[2];
      ierr = create_camera_point_is(&iss[0], &iss[1]);CHKERRQ(ierr);
      PetscInt n_camera, N_camera, n_point, N_point;
      ierr = ISGetSize(iss[0], &N_camera);CHKERRQ(ierr);
      ierr = ISGetLocalSize(iss[0], &n_camera);CHKERRQ(ierr);
      ierr = ISGetSize(iss[1], &N_point);CHKERRQ(ierr);
      ierr = ISGetLocalSize(iss[1], &n_point);CHKERRQ(ierr);

      Mat mats[2];
      ierr = MatCreate(comm, &mats[0]);CHKERRQ(ierr);
      ierr = MatSetBlockSizes(mats[0], 2, camera_block_size());CHKERRQ(ierr);
      ierr = MatSetSizes(mats[0], m, n_camera, M, N_camera);CHKERRQ(ierr);
      ierr = MatSetBlockSizes(mats[0], 1, 9);CHKERRQ(ierr);
      ierr = MatSetFromOptions(mats[0]);CHKERRQ(ierr);
      ierr = MatAIJSetPreallocationCSR(mats[0], row_ptr_camera.data(),
                                       cols_camera.data(), NULL);CHKERRQ(ierr);

      ierr = MatCreate(comm, &mats[1]);CHKERRQ(ierr);
      ierr = MatSetBlockSizes(mats[1], 2, point_block_size());CHKERRQ(ierr);
      ierr = MatSetSizes(mats[1], m, n_point, M, N_point);CHKERRQ(ierr);
      ierr = MatSetBlockSizes(mats[1], 1, 3);CHKERRQ(ierr);
      ierr = MatSetFromOptions(mats[1]);CHKERRQ(ierr);
      ierr = MatAIJSetPreallocationCSR(mats[1], row_ptr_point.data(),
                                       cols_point.data(), NULL);CHKERRQ(ierr);

      ierr = MatNestSetSubMats(*J, 1, NULL, 2, iss, mats);CHKERRQ(ierr);
      ierr = MatNestSetVecType(*J, VECSTANDARD);CHKERRQ(ierr);
      ierr = MatSetUp(*J);CHKERRQ(ierr);
    } else {
      ierr = ISLocalToGlobalMappingApply(col_l2g, cols.size(), cols.data(),
                                         cols.data());CHKERRQ(ierr);
      ierr = MatAIJSetPreallocationCSR(*J, row_ptr.data(), cols.data(), NULL);CHKERRQ(ierr);
    }

    PetscFunctionReturn(0);
  }

  PetscErrorCode gather_local_component(Vec parameters, Vec local_vec) const {
    PetscErrorCode ierr;
    PetscFunctionBegin;

    if (in_serial) {
      ierr = VecCopy(parameters, local_vec);CHKERRQ(ierr);
    } else {
      ierr = DMGlobalToLocalBegin(dm, parameters, INSERT_VALUES, local_vec);CHKERRQ(ierr);
      ierr = DMGlobalToLocalEnd(dm, parameters, INSERT_VALUES, local_vec);CHKERRQ(ierr);
    }

    PetscFunctionReturn(0);
  }

  // Compute the jacobian into the Mat J. J must have been created with
  // create_jacobian.
  PetscErrorCode compute(Vec parameters, Mat J, Vec residuals,
                         PetscReal *cost) const {
    PetscErrorCode ierr;
    PetscFunctionBegin;

    bool compute_jacobian = J != NULL;
    bool compute_cost = cost != NULL;
    bool compute_residuals = residuals != NULL;

    // Get local component of global vector.
    Vec local_parameters_vec;
    ierr = DMGetCoordinatesLocal(dm, &local_parameters_vec);CHKERRQ(ierr);
    ierr = gather_local_component(parameters, local_parameters_vec);CHKERRQ(ierr);

    PetscSection local_section;
    ierr = DMGetCoordinateSection(dm, &local_section);CHKERRQ(ierr);

    double *local_parameters;
    ierr = VecGetArray(local_parameters_vec, &local_parameters);CHKERRQ(ierr);

    // Assume residuals has the correct layout
    double *local_residuals;
    if (compute_residuals) {
      ierr = VecGetArray(residuals, &local_residuals);CHKERRQ(ierr);
    }

    // preallocate jacobian and residual blocks as their sizes do not change.
    std::vector<double> jacobian_block(residual_block_size() * parameter_size(),
                                       1.0 / 0.0);
    std::vector<double *> jacobian_ptrs =
        create_jacobian_ptrs(jacobian_block.data());
    std::vector<double> residual_block(residual_block_size());

    // Used for MatSetValues
    std::vector<PetscInt> parameter_offsets(parameter_size());
    std::vector<PetscInt> residual_offsets(residual_block_size());

    double local_cost = 0;

    PetscInt residual_offset;
    if (compute_jacobian) {
      ierr = MatGetOwnershipRange(J, &residual_offset, NULL);CHKERRQ(ierr);
    }

    bool is_nest = false;
    if (compute_jacobian) {
      MatType type;
      ierr = MatGetType(J, &type);CHKERRQ(ierr);
      is_nest = std::string(type) == MATNEST;
    }

    // Compute the jacobian block by block.
    for (PetscInt i = 0; i < num_local_residual_blocks(); i++) {
      // Collect parameters
      PetscInt camera_off, point_off;
      ierr = PetscSectionGetOffset(local_section, costs[i].camera, &camera_off);CHKERRQ(ierr);
      ierr = PetscSectionGetOffset(local_section, costs[i].point, &point_off);CHKERRQ(ierr);
      std::vector<double *> parameter_ptrs = {local_parameters + camera_off,
                                              local_parameters + point_off};
      double block_cost;
      if (!costs[i].Evaluate(
              local_parameters + camera_off, local_parameters + point_off,
              &block_cost, residual_block.data(),
              compute_jacobian ? jacobian_ptrs.data() : NULL)) {
        SETERRQ(PETSC_COMM_SELF, PETSC_ERR_LIB,
                "Ceres failed to compute residual");
      }

      local_cost += block_cost;

      if (compute_jacobian) {
        // Get parameter offsets
        PetscInt camera_dof, point_dof;
        ierr = PetscSectionGetDof(local_section, costs[i].camera, &camera_dof);CHKERRQ(ierr);
        for (PetscInt j = 0; j < camera_dof; j++) {
          if (is_nest) {
            parameter_offsets[j] =
                camera_or_point_global_offset(costs[i].camera) + j;
          } else {
            parameter_offsets[j] = camera_off + j;
          }
        }

        ierr = PetscSectionGetDof(local_section, costs[i].point, &point_dof);CHKERRQ(ierr);
        for (PetscInt j = 0; j < point_dof; j++) {
          if (is_nest) {
            parameter_offsets[camera_dof + j] =
                camera_or_point_global_offset(costs[i].point) + j;
          } else {
            parameter_offsets[camera_dof + j] = point_off + j;
          }
        }

        // Get residual_offsets
        for (PetscInt j = 0; j < residual_block_size(); j++) {
          residual_offsets[j] = residual_offset + residual_block_size() * i + j;
        }

        if (is_nest) {
          Mat camera_mat, point_mat;

          ierr = MatNestGetSubMat(J, 0, 0, &camera_mat);CHKERRQ(ierr);
          ierr = MatSetValues(camera_mat, residual_offsets.size(),
                              residual_offsets.data(), camera_block_size(),
                              parameter_offsets.data(), jacobian_block.data(),
                              INSERT_VALUES);CHKERRQ(ierr);

          ierr = MatNestGetSubMat(J, 0, 1, &point_mat);CHKERRQ(ierr);
          ierr = MatSetValues(point_mat, residual_offsets.size(),
                              residual_offsets.data(), point_block_size(),
                              parameter_offsets.data() + camera_block_size(),
                              jacobian_block.data() +
                                  camera_block_size() * residual_block_size(),
                              INSERT_VALUES);CHKERRQ(ierr);
        } else {
          ierr = ISLocalToGlobalMappingApply(col_l2g, parameter_offsets.size(),
                                             parameter_offsets.data(),
                                             parameter_offsets.data());CHKERRQ(ierr);

          ierr =
              MatSetValues(J, residual_offsets.size(), residual_offsets.data(),
                           camera_block_size(), parameter_offsets.data(),
                           jacobian_block.data(), INSERT_VALUES);CHKERRQ(ierr);
          ierr = MatSetValues(J, residual_offsets.size(),
                              residual_offsets.data(), point_block_size(),
                              parameter_offsets.data() + camera_block_size(),
                              jacobian_block.data() +
                                  camera_block_size() * residual_block_size(),
                              INSERT_VALUES);CHKERRQ(ierr);
        }
      }

      if (compute_residuals) {
        std::copy_n(residual_block.begin(), residual_block_size(),
                    local_residuals + i * residual_block_size());
      }
    }

    ierr = VecRestoreArray(local_parameters_vec, &local_parameters);CHKERRQ(ierr);
    if (compute_residuals) {
      ierr = VecRestoreArray(residuals, &local_residuals);CHKERRQ(ierr);
    }

    // Sum up all local costs
    if (compute_cost) {
      MPI_Allreduce(&local_cost, cost, 1, MPIU_REAL, MPI_SUM, comm);
    }

    if (compute_jacobian) {
      ierr = MatAssemblyBegin(J, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
      ierr = MatAssemblyEnd(J, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

      // Set nullspace
      MatNullSpace nullsp;
      IS iss[2];
      ierr = MatNestGetISs(J, NULL, iss);CHKERRQ(ierr);
      Vec cameras_vec;
      ierr = VecGetSubVector(parameters, iss[0], &cameras_vec);CHKERRQ(ierr);
      // TODO: don't want to reallocate vectors every iteration
      ierr = create_nullspace(cameras_vec, true, &nullsp);CHKERRQ(ierr);
      ierr = VecRestoreSubVector(parameters, iss[0], &cameras_vec);CHKERRQ(ierr);
      PetscObjectCompose((PetscObject) iss[0], "nearnullspace", (PetscObject) nullsp);
    }

    PetscFunctionReturn(0);
  }

  // Mapping from the index of a camera or point in the local section to its
  // offset in either a global camera vector or a global point vector.
  PetscInt camera_or_point_global_offset(PetscInt point) const {
    return local_indices.at(point);
  }

  // IS of cameras and points in the global section
  // TODO: use DMPlexGetDepthStratum
  PetscErrorCode create_camera_point_is(IS *is_camera, IS *is_point) const {
    PetscErrorCode ierr;
    PetscFunctionBegin;

    PetscSection section;
    ierr = DMGetGlobalSection(dm, &section);CHKERRQ(ierr);
    PetscInt p_start, p_end;
    ierr = PetscSectionGetChart(section, &p_start, &p_end);CHKERRQ(ierr);
    std::vector<PetscInt> camera_inds;
    std::vector<PetscInt> point_inds;
    for (PetscInt i = p_start; i < p_end; i++) {
      PetscInt dof, off;
      ierr = PetscSectionGetDof(section, i, &dof);CHKERRQ(ierr);
      ierr = PetscSectionGetOffset(section, i, &off);CHKERRQ(ierr);
      // Skip non-local points.
      if (off < 0) {
        continue;
      }
      if (dof == camera_block_size()) {
        for (PetscInt j = 0; j < dof; j++) {
          camera_inds.push_back(off + j);
        }
      } else {
        for (PetscInt j = 0; j < dof; j++) {
          point_inds.push_back(off + j);
        }
      }
    }

    ierr = ISCreateGeneral(comm, camera_inds.size(), camera_inds.data(),
                           PETSC_COPY_VALUES, is_camera);CHKERRQ(ierr);
    ierr = ISViewFromOptions(*is_camera, NULL, "-is_camera_view");CHKERRQ(ierr);
    ierr = ISCreateGeneral(comm, point_inds.size(), point_inds.data(),
                           PETSC_COPY_VALUES, is_point);CHKERRQ(ierr);
    ierr = ISViewFromOptions(*is_point, NULL, "-is_point_view");CHKERRQ(ierr);

    PetscFunctionReturn(0);
  }

  // Copy Tao solution to BALProblem on the first processor
  PetscErrorCode copy_to_bal(Tao tao, BALProblem &problem) {
    PetscErrorCode ierr;
    PetscFunctionBegin;

    // We have to put the parameters back in the correct order
    Vec solution, natural_solution;
    ierr = TaoGetSolutionVector(tao, &solution);CHKERRQ(ierr);
    ierr = VecDuplicate(solution, &natural_solution);CHKERRQ(ierr);
    ierr = DMPlexGlobalToNaturalBegin(dm, solution, natural_solution);CHKERRQ(ierr);
    ierr = DMPlexGlobalToNaturalEnd(dm, solution, natural_solution);CHKERRQ(ierr);

    double* ps;
    VecGetArray(solution, &ps);
    VecRestoreArray(solution, &ps);

    // Gather vector to first processor
    Vec gathered_params;
    int rank;
    MPI_Comm_rank(comm, &rank);
    PetscInt N;
    if (rank == 0) {
      ierr = VecGetSize(natural_solution, &N);CHKERRQ(ierr);
    } else {
      N = 0;
    }
    ierr = VecCreateSeq(PETSC_COMM_SELF, N, &gathered_params);CHKERRQ(ierr);
    VecScatter gather;
    ierr = VecScatterCreateToZero(natural_solution, &gather, &gathered_params);CHKERRQ(ierr);

    ierr = VecScatterBegin(gather, natural_solution, gathered_params,
                           INSERT_VALUES, SCATTER_FORWARD);CHKERRQ(ierr);
    ierr = VecScatterEnd(gather, natural_solution, gathered_params,
                         INSERT_VALUES, SCATTER_FORWARD);CHKERRQ(ierr);

    const PetscReal *gathered_params_local;
    ierr = VecGetArrayRead(gathered_params, &gathered_params_local);CHKERRQ(ierr);
    std::copy_n(gathered_params_local, N, problem.mutable_parameters());
    ierr = VecRestoreArrayRead(gathered_params, &gathered_params_local);CHKERRQ(ierr);

    PetscFunctionReturn(0);
  }

  // TODO: use the correct way to index this instead of assuming the order or parameters
  PetscErrorCode create_nullspace(Vec camera_params, bool use_constant_vecs, MatNullSpace* nullsp) const {
    PetscErrorCode ierr;
    PetscFunctionBegin;

    const PetscScalar* sol;
    ierr = VecGetArrayRead(camera_params, &sol);CHKERRQ(ierr);

    // null space vectors
    // x translation
    // y translation
    // z translation
    // scaling
    // x rotation
    // y rotation
    // z rotation
    int num_vecs = use_constant_vecs ? 16 : 7;
    std::vector<Vec> null_vecs(num_vecs);
    for(int i = 0; i < null_vecs.size(); i++) {
      ierr = VecCreate(comm, &null_vecs[i]);CHKERRQ(ierr);
      ierr = VecSetSizes(null_vecs[i], num_local_cameras()*camera_block_size(), num_global_cameras()*camera_block_size());CHKERRQ(ierr);
      ierr = VecSetFromOptions(null_vecs[i]);CHKERRQ(ierr);
      ierr = VecSet(null_vecs[i], 0);CHKERRQ(ierr);
    }

    PetscScalar* x_trans;
    ierr = VecGetArray(null_vecs[0], &x_trans);CHKERRQ(ierr);
    PetscScalar* y_trans;
    ierr = VecGetArray(null_vecs[1], &y_trans);CHKERRQ(ierr);
    PetscScalar* z_trans;
    ierr = VecGetArray(null_vecs[2], &z_trans);CHKERRQ(ierr);
    PetscScalar* scaling;
    ierr = VecGetArray(null_vecs[3], &scaling);CHKERRQ(ierr);
    PetscScalar* x_rot;
    ierr = VecGetArray(null_vecs[4], &x_rot);CHKERRQ(ierr);
    PetscScalar* y_rot;
    ierr = VecGetArray(null_vecs[5], &y_rot);CHKERRQ(ierr);
    PetscScalar* z_rot;
    ierr = VecGetArray(null_vecs[6], &z_rot);CHKERRQ(ierr);

    for(PetscInt i = 0; i < num_local_cameras(); i++) {
      PetscInt off = i * camera_block_size();
      // camera translations
      std::array<PetscScalar, 3> trans_x = {1, 0, 0};
      ceres::AngleAxisRotatePoint(&sol[off], trans_x.data(), &x_trans[off+3]);
      for(int j = 3; j < 6; j++) x_trans[off+j] *= -1;
      std::array<PetscScalar, 3> trans_y = {0, 1, 0};
      ceres::AngleAxisRotatePoint(&sol[off], trans_y.data(), &y_trans[off+3]);
      for(int j = 3; j < 6; j++) y_trans[off+j] *= -1;
      std::array<PetscScalar, 3> trans_z = {0, 0, 1};
      ceres::AngleAxisRotatePoint(&sol[off], trans_z.data(), &z_trans[off+3]);
      for(int j = 3; j < 6; j++) z_trans[off+j] *= -1;

      // scaling
      scaling[off+3] = sol[off+3];
      scaling[off+4] = sol[off+4];
      scaling[off+5] = sol[off+5];

      // rotations
      linearized_rotation(&sol[off], {1, 0, 0}, &x_rot[off]);
      linearized_rotation(&sol[off], {0, 1, 0}, &y_rot[off]);
      linearized_rotation(&sol[off], {0, 0, 1}, &z_rot[off]);
    }

    ierr = VecRestoreArrayRead(camera_params, &sol);CHKERRQ(ierr);
    ierr = VecRestoreArray(null_vecs[0], &x_trans);CHKERRQ(ierr);
    ierr = VecRestoreArray(null_vecs[1], &y_trans);CHKERRQ(ierr);
    ierr = VecRestoreArray(null_vecs[2], &z_trans);CHKERRQ(ierr);
    ierr = VecRestoreArray(null_vecs[3], &scaling);CHKERRQ(ierr);
    ierr = VecRestoreArray(null_vecs[4], &x_rot);CHKERRQ(ierr);
    ierr = VecRestoreArray(null_vecs[5], &y_rot);CHKERRQ(ierr);
    ierr = VecRestoreArray(null_vecs[6], &z_rot);CHKERRQ(ierr);

    // MatNullSpace requires norm 1 vectors
    for(int i = 0; i < 7; i++) {
      ierr = VecNormalize(null_vecs[i], NULL);CHKERRQ(ierr);
    }

    // add constant vectors
    if(use_constant_vecs) {
      for(int i = 0; i < 9; i++) {
        PetscScalar *ary;
        Vec v = null_vecs[i+7];
        ierr = VecGetArray(v, &ary);CHKERRQ(ierr);
        for(int j = 0; j < num_local_cameras(); j++) {
          for(int k = 0; k < 9; k++) ary[j*camera_block_size()+k] = 0;
          ary[j*camera_block_size()+i] = 1;
        }
        ierr = VecRestoreArray(v, &ary);CHKERRQ(ierr);
      }
    }

    // orthogonalize
    for(int i = 1; i < null_vecs.size(); i++) {
      Vec v = null_vecs[i];
      for(int j = 0; j < i; j++) {
        PetscScalar d;
        ierr = VecDot(null_vecs[j], v, &d);CHKERRQ(ierr);
        ierr = VecAXPY(v, -d, null_vecs[j]);CHKERRQ(ierr);
        ierr = VecNormalize(v, NULL);CHKERRQ(ierr);
      }
    }
    // orthogonalize again
    for(int i = 1; i < null_vecs.size(); i++) {
      Vec v = null_vecs[i];
      for(int j = 0; j < i; j++) {
        PetscScalar d;
        ierr = VecDot(null_vecs[j], v, &d);CHKERRQ(ierr);
        ierr = VecAXPY(v, -d, null_vecs[j]);CHKERRQ(ierr);
        ierr = VecNormalize(v, NULL);CHKERRQ(ierr);
      }
    }

    ierr = MatNullSpaceCreate(comm, PETSC_FALSE, null_vecs.size(), null_vecs.data(), nullsp);CHKERRQ(ierr);
    for(int i = 0; i < null_vecs.size(); i++) {
      ierr = VecDestroy(&null_vecs[i]);CHKERRQ(ierr);
    }

    PetscFunctionReturn(0);
  }

  PetscInt num_local_residual_blocks() const { return costs.size(); }

  PetscInt residual_block_size() const {
    return costs.at(0).num_residuals();
  }

  PetscInt parameter_size() const {
    PetscInt size = 0;
    for (auto i :
         costs.at(0).f->parameter_block_sizes()) {
      size += i;
    }
    return size;
  }

  PetscInt num_local_residuals() const {
    return residual_block_size() * num_local_residual_blocks();
  }

  std::vector<double *> create_jacobian_ptrs(double *jacobian_block) const {
    return {jacobian_block,
            jacobian_block + residual_block_size() * camera_block_size()};
  }

  PetscInt camera_block_size() const {
    return costs[0].camera_block_size();
  }

  PetscInt point_block_size() const {
    return costs[0].point_block_size();
  }

  PetscInt num_local_cameras() const {
    PetscInt local_size;
    PETSC_CALL(ISGetLocalSize(camera_is, &local_size));
    return local_size;
  }

  PetscInt num_global_cameras() const {
    PetscInt global_size;
    PETSC_CALL(ISGetSize(camera_is, &global_size));
    return global_size;
  }

  // Forbid copying as we have a reference to the param_blocks;
  DistributedProblem(const DistributedProblem &) = delete;

  ~DistributedProblem() {
    DMDestroy(&dm);
    VecDestroy(&residuals_vec);
  }

  MPI_Comm comm;
  DM dm;
  ISLocalToGlobalMapping
      col_l2g;  // borrowed reference to dm's local to global mapping
  std::vector<Cost> costs;
  Vec residuals_vec;
  IS residual_global_to_natural;
  IS camera_is;
  IS point_is;
  std::vector<PetscInt> local_indices;
  bool in_serial;
};

// Wrapper to hold DistributedProblem and a BALProblem used for verification.
// TODO: this definitely needs to be cleaned up.
struct TaoBALProblem {
  // BAL problem used for verification. NULL if verification is not being
  // performed.
  nonstd::optional<BALProblem> bal_problem;
  std::unique_ptr<ceres::Problem> ceres_problem;
  DistributedProblem *problem;
  Mat cached_j;  // Cached jacobian matrix
};

PetscErrorCode tao_cost_shim(Tao tao, Vec x, PetscReal *f, void *ctx) {
  PetscErrorCode ierr;
  PetscFunctionBegin;

  TaoBALProblem *prob = static_cast<TaoBALProblem *>(ctx);

  ierr = prob->problem->compute(x, NULL, NULL, f);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

PetscErrorCode tao_cost_gradient_shim(Tao tao, Vec x, PetscReal *f, Vec g,
                                      void *ctx) {
  PetscErrorCode ierr;
  PetscFunctionBegin;

  TaoBALProblem *prob = static_cast<TaoBALProblem *>(ctx);

  // TODO: Unnessisary recomputation of J
  ierr = prob->problem->compute(x, prob->cached_j, prob->problem->residuals_vec,
                                f);CHKERRQ(ierr);

  ierr = MatMultTranspose(prob->cached_j, prob->problem->residuals_vec, g);CHKERRQ(ierr);

  if (prob->bal_problem) {
    PetscInt rank;
    MPI_Comm_rank(PetscObjectComm((PetscObject)tao), &rank);
    prob->problem->copy_to_bal(tao, *prob->bal_problem);

    double cost;
    std::vector<double> gradient;
    std::vector<double> residuals;
    ceres::Problem::EvaluateOptions eval_opts;
    ceres::CRSMatrix jacobian;
    prob->ceres_problem->Evaluate(eval_opts, &cost, &residuals, &gradient,
                                  &jacobian);

    // compare residuals
    Vec residuals_vec;
    ierr = VecCreateSeqWithArray(PETSC_COMM_SELF, 1, residuals.size(),
                                 residuals.data(), &residuals_vec);CHKERRQ(ierr);
    ierr = VecViewFromOptions(prob->problem->residuals_vec, NULL,
                              "-residuals_view");CHKERRQ(ierr);
    ierr = VecViewFromOptions(residuals_vec, NULL, "-ceres_residuals_view");CHKERRQ(ierr);
    ierr = VecAXPY(residuals_vec, -1, prob->problem->residuals_vec);CHKERRQ(ierr);
    ierr = VecViewFromOptions(residuals_vec, NULL, "-residuals_diff_view");CHKERRQ(ierr);
    PetscScalar residual_norm;
    ierr = VecNorm(residuals_vec, NORM_INFINITY, &residual_norm);CHKERRQ(ierr);
    PetscPrintf(prob->problem->comm, "Residuals L_inf difference: %e\n",
                residual_norm);

    // Compare jacobians
    Mat J_est;
    ierr = MatCreate(PETSC_COMM_SELF, &J_est);CHKERRQ(ierr);
    ierr = MatSetSizes(J_est, PETSC_DECIDE, PETSC_DECIDE, jacobian.num_rows,
                       jacobian.num_cols);CHKERRQ(ierr);
    ierr = MatSetType(J_est, MATSEQAIJ);CHKERRQ(ierr);
    ierr = MatSeqAIJSetPreallocationCSR(J_est, jacobian.rows.data(),
                                        jacobian.cols.data(),
                                        jacobian.values.data());CHKERRQ(ierr);
    ierr = MatViewFromOptions(J_est, NULL, "-ceres_jac_view");CHKERRQ(ierr);
    ierr = MatViewFromOptions(prob->cached_j, NULL, "-jac_view");CHKERRQ(ierr);
    ierr = MatAXPY(J_est, -1, prob->cached_j, SAME_NONZERO_PATTERN);CHKERRQ(ierr);
    PetscReal J_norm;
    ierr = MatNorm(J_est, NORM_INFINITY, &J_norm);CHKERRQ(ierr);

    PetscPrintf(prob->problem->comm, "Jacobian L_inf difference: %e\n", J_norm);

    Vec grad, grad_dist, solution_vec;
    VecScatter scatter;
    ierr = TaoGetSolutionVector(tao, &solution_vec);CHKERRQ(ierr);
    ierr = VecDuplicate(g, &grad_dist);CHKERRQ(ierr);
    if (rank == 0) {
      ierr = VecCreateSeqWithArray(PETSC_COMM_SELF, 1, gradient.size(),
                                   gradient.data(), &grad);CHKERRQ(ierr);
    } else {
      ierr = VecCreateSeq(PETSC_COMM_SELF, 0, &grad);CHKERRQ(ierr);
    }

    ierr = VecScatterCreateToZero(grad_dist, &scatter, NULL);CHKERRQ(ierr);
    ierr = VecScatterBegin(scatter, grad, grad_dist, INSERT_VALUES,
                           SCATTER_REVERSE);CHKERRQ(ierr);
    ierr =
        VecScatterEnd(scatter, grad, grad_dist, INSERT_VALUES, SCATTER_REVERSE);CHKERRQ(ierr);

    // Return to ceres ordering
    // VecPermute only supports local orderings
    // ierr = VecPermute(grad_dist, prob->problem->residual_global_to_natural,
    // PETSC_FALSE);CHKERRQ(ierr);
    Vec grad_permuted;
    ierr = VecDuplicate(grad_dist, &grad_permuted);CHKERRQ(ierr);
    ierr = VecCopy(grad_dist, grad_permuted);CHKERRQ(ierr);
    // ierr = VecISCopy(grad_permuted,
    // prob->problem->residual_global_to_natural,
    //                  SCATTER_BACKWARD, grad_dist);CHKERRQ(ierr);

    ierr = VecViewFromOptions(grad_permuted, NULL, "-ceres_grad_view");CHKERRQ(ierr);
    ierr = VecViewFromOptions(g, NULL, "-grad_view");CHKERRQ(ierr);

    ierr = VecAXPY(grad_permuted, -1, g);CHKERRQ(ierr);
    PetscReal grad_norm;
    ierr = VecNorm(grad_permuted, NORM_INFINITY, &grad_norm);CHKERRQ(ierr);
    PetscPrintf(prob->problem->comm, "Gradient L_inf difference: %e\n",
                grad_norm);
    PetscPrintf(prob->problem->comm,
                "Cost difference: %e (ceres: %e, tao: %e)\n", *f - cost, cost,
                *f);

    VecScatterDestroy(&scatter);
    VecDestroy(&grad_dist);
    VecDestroy(&grad);
  }

  PetscFunctionReturn(0);
}

PetscErrorCode tao_hessian_shim(Tao tao, Vec x, Mat H, Mat Hpre, void *ctx) {
  PetscErrorCode ierr;
  PetscFunctionBegin;

  TaoBALProblem *prob = static_cast<TaoBALProblem *>(ctx);

  ierr = prob->problem->compute(x, prob->cached_j, NULL, NULL);CHKERRQ(ierr);

  ierr = MatTransposeMatMult(prob->cached_j, prob->cached_j, MAT_REUSE_MATRIX,
                             PETSC_DEFAULT, &H);CHKERRQ(ierr);

  // PetscScalar lambda;
  // ierr = TaoGetCurrentTrustRegionRadius(tao, &lambda);CHKERRQ(ierr);
  // Vec d;
  // ierr = VecDuplicate(x, &d);CHKERRQ(ierr);
  // ierr = VecSet(d, lambda);CHKERRQ(ierr);
  // ierr = MatDiagonalSet(H, d, ADD_VALUES);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

PetscErrorCode tao_jacobian_shim(Tao tao, Vec x, Mat J, Mat Jpre, void *ctx) {
  PetscErrorCode ierr;
  PetscFunctionBegin;

  TaoBALProblem *prob = static_cast<TaoBALProblem *>(ctx);

  ierr = prob->problem->compute(x, J, NULL, NULL);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

PetscErrorCode tao_residual_shim(Tao tao, Vec x, Vec r, void *ctx) {
  PetscErrorCode ierr;
  PetscFunctionBegin;

  TaoBALProblem *prob = static_cast<TaoBALProblem *>(ctx);

  // TODO: should have an objective and residuals routine
  ierr = prob->problem->compute(x, NULL, r, NULL);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

PetscErrorCode tao_ba_create_hessian(const DistributedProblem &problem, Mat J,
                                     Mat *H) {
  PetscErrorCode ierr;
  PetscFunctionBegin;

  PetscInt m, M;
  ierr = MatGetSize(J, NULL, &M);CHKERRQ(ierr);
  ierr = MatGetLocalSize(J, NULL, &m);CHKERRQ(ierr);

  ierr = MatCreate(problem.comm, H);CHKERRQ(ierr);
  ierr = MatSetSizes(*H, m, m, M, M);CHKERRQ(ierr);
  ierr = MatSetOptionsPrefix(*H, "ba_hessian_");CHKERRQ(ierr);
  ierr = MatSetFromOptions(*H);CHKERRQ(ierr);
  // We must use MATNEST so that the block sizes are propogated correctly
  ierr = MatSetType(*H, MATNEST);CHKERRQ(ierr);

  MatType mat_type;
  ierr = MatGetType(*H, &mat_type);CHKERRQ(ierr);
  MatType jac_type;
  ierr = MatGetType(J, &jac_type);CHKERRQ(ierr);
  if (std::string(mat_type) == MATSHELL) {
    SETERRQ(problem.comm, PETSC_ERR_LIB, "Unimplemented");
  } else if (std::string(mat_type) == MATNEST) {
    Mat submats[4];
    IS row_is[1];  // rows_is[0] = all rows
    IS col_is[2];  // cols_is = {points, cameras}
    ierr = MatNestGetISs(J, row_is, col_is);CHKERRQ(ierr);
    Mat mat_camera, mat_point;
    ierr = MatNestGetSubMat(J, 0, 0, &mat_point);CHKERRQ(ierr);
    ierr = MatNestGetSubMat(J, 0, 1, &mat_camera);CHKERRQ(ierr);

    // create submats
    ierr = MatTransposeMatMult(mat_point, mat_point, MAT_INITIAL_MATRIX,
                               PETSC_DEFAULT, &submats[0]);CHKERRQ(ierr);
    ierr = MatTransposeMatMult(mat_point, mat_camera, MAT_INITIAL_MATRIX,
                               PETSC_DEFAULT, &submats[1]);CHKERRQ(ierr);
    ierr = MatTransposeMatMult(mat_camera, mat_point, MAT_INITIAL_MATRIX,
                               PETSC_DEFAULT, &submats[2]);CHKERRQ(ierr);
    ierr = MatTransposeMatMult(mat_camera, mat_camera, MAT_INITIAL_MATRIX,
                               PETSC_DEFAULT, &submats[3]);CHKERRQ(ierr);
    ierr = MatCreateNest(PetscObjectComm((PetscObject)J), 2, col_is, 2, col_is,
                         submats, H);CHKERRQ(ierr);
  } else {
    ierr = MatTransposeMatMult(J, J, MAT_INITIAL_MATRIX, PETSC_DEFAULT, H);CHKERRQ(ierr);
  }

  PetscFunctionReturn(0);
}

PetscErrorCode tao_ba_set_preconditioner(const TaoBALProblem &problem,
                                         Tao tao) {
  PetscErrorCode ierr;
  IS is_camera, is_point;
  PetscFunctionBegin;

  problem.problem->create_camera_point_is(&is_camera, &is_point);

  KSP ksp;
  PC pc;
  ierr = TaoGetKSP(tao, &ksp);CHKERRQ(ierr);
  if (ksp != NULL) {
    ierr = KSPGetPC(ksp, &pc);CHKERRQ(ierr);

    IS is[2];
    ierr = MatNestGetISs(problem.cached_j, NULL, is);CHKERRQ(ierr);
    ierr = PCFieldSplitSetIS(pc, "point", is[1]);CHKERRQ(ierr);
    ierr = PCFieldSplitSetIS(pc, "camera", is[0]);CHKERRQ(ierr);
  }

  PetscFunctionReturn(0);
}

PetscErrorCode TaoMonitorAll(Tao tao, void *) {
  PetscErrorCode ierr;
  PetscFunctionBegin;

  Vec gradient;
  PetscReal norm;
  PetscReal trust_size;
  ierr = TaoGetGradientVector(tao, &gradient);CHKERRQ(ierr);
  ierr = VecNorm(gradient, NORM_INFINITY, &norm);CHKERRQ(ierr);
  ierr = TaoGetCurrentTrustRegionRadius(tao, &trust_size);CHKERRQ(ierr);
  PetscInt niter;
  ierr = TaoGetIterationNumber(tao, &niter);CHKERRQ(ierr);
  PetscScalar f;
  ierr = TaoGetObjective(tao, &f);CHKERRQ(ierr);

  PetscReal* fs;
  PetscReal* res;
  PetscInt n;
  ierr = TaoGetConvergenceHistory(tao, &fs, &res, NULL, NULL, &n);CHKERRQ(ierr);

  PetscReal cost_change = 0;
  if(n > 1) {
    // find the last good solution
    auto min_cost = *std::min_element(fs, fs+n-1);
    cost_change = min_cost - f;
  }

  PetscPrintf(PetscObjectComm((PetscObject)tao),
              "%4d TAO, Function value: %10e, Cost change: % 10e, |Gradient|: %10e, Trust: %10e\n",
              niter, f, cost_change, norm, trust_size);

  PetscFunctionReturn(0);
}

PetscErrorCode create_tao(DistributedProblem *problem, Tao *tao,
                          nonstd::optional<BALProblem> &&bal_problem,
                          std::unique_ptr<ceres::Problem> &&ceres_problem) {
  PetscErrorCode ierr;
  PetscFunctionBegin;

  Mat J;
  ierr = problem->create_jacobian(&J);CHKERRQ(ierr);
  ierr = MatSetOption(J, MAT_NO_OFF_PROC_ENTRIES, PETSC_TRUE);CHKERRQ(ierr);
  TaoBALProblem *wrapped_problem = new TaoBALProblem{
      std::move(bal_problem), std::move(ceres_problem), problem, J};

  PetscInt m, M;
  ierr = MatGetSize(J, NULL, &M);CHKERRQ(ierr);
  ierr = MatGetLocalSize(J, NULL, &m);CHKERRQ(ierr);

  // Mat H;
  // ierr = tao_ba_create_hessian(*problem, J, &H);CHKERRQ(ierr);

  ierr = TaoCreate(problem->comm, tao);CHKERRQ(ierr);
  ierr = TaoSetObjectiveAndGradientRoutine(*tao, tao_cost_gradient_shim,
                                           wrapped_problem);CHKERRQ(ierr);
  ierr = TaoSetObjectiveRoutine(*tao, tao_cost_shim, wrapped_problem);CHKERRQ(ierr);
  // ierr = TaoSetHessianRoutine(*tao, H, H, tao_hessian_shim,
  // wrapped_problem);CHKERRQ(ierr);
  ierr = TaoSetJacobianResidualRoutine(*tao, J, J, tao_jacobian_shim, wrapped_problem);CHKERRQ(ierr);
  ierr = TaoSetResidualRoutine(*tao, wrapped_problem->problem->residuals_vec,
                               tao_residual_shim, wrapped_problem);CHKERRQ(ierr);
  Vec param_vec, initial;
  ierr = problem->get_parameter_vec(&param_vec);CHKERRQ(ierr);
  ierr = VecDuplicate(param_vec, &initial);CHKERRQ(ierr);
  ierr = VecCopy(param_vec, initial);CHKERRQ(ierr);
  ierr = TaoSetInitialVector(*tao, initial);CHKERRQ(ierr);

  // Tell petsc to store convergence history
  ierr = TaoSetConvergenceHistory(*tao, NULL, NULL, NULL, NULL, PETSC_DECIDE, PETSC_TRUE);CHKERRQ(ierr);
  ierr = TaoSetMonitor(*tao, TaoMonitorAll, NULL, NULL);CHKERRQ(ierr);

  ierr = TaoSetFromOptions(*tao);CHKERRQ(ierr);
  ierr = TaoSetUp(*tao);CHKERRQ(ierr);

  ierr = tao_ba_set_preconditioner(*wrapped_problem, *tao);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}
