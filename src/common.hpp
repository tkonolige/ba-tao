#pragma once

#include <ceres/ceres.h>
#include "bal_problem.h"
#include "snavely_reprojection_error.h"
#include <unistd.h>
#include <fstream>

// If PETSc calls this code, which call PETSc, which throws an error, the error
// will not be handled correctly. TODO: fix this
struct petsc_error : std::runtime_error {
  petsc_error(std::string msg) : std::runtime_error(msg) {}
};

// from
// https://github.com/geodynamics/pylith/blob/master/libsrc/pylith/utils/error.h
// TODO: get error message
#define PETSC_CHECK_ERROR(err)                                             \
  do {                                                                     \
    if (PetscUnlikely(err)) {                                              \
      PetscError(PETSC_COMM_SELF, __LINE__, PETSC_FUNCTION_NAME, __FILE__, \
                 err, PETSC_ERROR_REPEAT, 0);                              \
      throw petsc_error("");                                                 \
    }                                                                      \
  } while (0)

#define PETSC_CALL(expr)     \
  {                          \
    PetscErrorCode ierr;     \
    ierr = (expr);           \
    PETSC_CHECK_ERROR(ierr); \
  }

// from ceres/exmaples/bundle_adjuster.cc
void BuildProblem(ceres::examples::BALProblem& bal_problem, ceres::Problem* problem,
                            std::shared_ptr<ceres::ParameterBlockOrdering>& ordering,
                            bool robust) {
  const int point_block_size = bal_problem.point_block_size();
  const int camera_block_size = bal_problem.camera_block_size();
  double* points = bal_problem.mutable_points();
  double* cameras = bal_problem.mutable_cameras();

  // Observations is 2*num_observations long array observations =
  // [u_1, u_2, ... , u_n], where each u_i is two dimensional, the x
  // and y positions of the observation.
  const double* observations = bal_problem.observations();

  // add all parameter blocks so even nonlocal parameters are used
  for (int i = 0; i < bal_problem.num_cameras(); i++) {
    problem->AddParameterBlock(cameras + camera_block_size * i,
                               camera_block_size);
    ordering->AddElementToGroup(cameras + camera_block_size * i, 1);
  }
  for (int i = 0; i < bal_problem.num_points(); i++) {
    problem->AddParameterBlock(points + point_block_size * i, point_block_size);
    ordering->AddElementToGroup(points + point_block_size * i, 0);
  }

  for (int i = 0; i < bal_problem.num_observations(); i++) {
    ceres::CostFunction* cost_function;
    // Each Residual block takes a point and a camera as input and
    // outputs a 2 dimensional residual.
    cost_function = ceres::examples::SnavelyReprojectionError::Create(observations[2 * i + 0],
                                                     observations[2 * i + 1]);

    // If enabled use Huber's loss function.
    ceres::LossFunction* loss_function = robust ? new ceres::HuberLoss(1.0) : NULL;

    // Each observation corresponds to a pair of a camera and a point
    // which are identified by camera_index()[i] and point_index()[i]
    // respectively.
    double* camera =
        cameras + camera_block_size * bal_problem.camera_index()[i];
    double* point = points + point_block_size * bal_problem.point_index()[i];
    problem->AddResidualBlock(cost_function, loss_function, camera, point);
  }

  // set the first camera as constant
  // problem->SetParameterBlockConstant(cameras);
}

std::string hostname() {
  char hnc[128];
  gethostname(hnc, 128);
  return std::string(hnc);
}

struct CSVCallback : ceres::IterationCallback {
  CSVCallback(const std::string& filepath, const std::string& solver, const ceres::examples::BALProblem& problem, const std::string& loss) : ofs(filepath), solver(solver), loss(loss), num_cameras(problem.num_cameras()), num_points(problem.num_points()), hn(hostname()) {}
  std::ofstream ofs;
  std::string solver;
  std::string loss;
  int64_t num_cameras;
  int64_t num_points;
  std::string hn;
  ceres::CallbackReturnType operator()(const ceres::IterationSummary& summary) {

    if (summary.iteration == 0) {
      ofs << "problem,solver,cost,cost_change,gradient_norm,step_norm,damping,iters,setup_time,solve_time,iter_time,eta,cameras,points,flops,loss,hostname" << std::endl;
    }
    ofs << summary.iteration << ","
        << solver << ","
        << summary.cost << ","
        << summary.cost_change << ","
        << summary.gradient_max_norm << ","
        << summary.step_norm << ","
        << 1.0/summary.trust_region_radius << ","
        << summary.linear_solver_iterations << ","
        << summary.step_solver_setup_time_in_seconds << ","
        << summary.step_solver_solve_time_in_seconds << ","
        << summary.iteration_time_in_seconds << ","
        << summary.eta << ","
        << num_cameras << ","
        << num_points << ","
        << summary.flops << ","
        << loss << ","
        << hn
        << std::endl;
    return ceres::SOLVER_CONTINUE;
  }

};
