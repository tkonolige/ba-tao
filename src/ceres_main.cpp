#include <ceres/ceres.h>
#include "common.hpp"
#include "bal_problem.h"
#include <gflags/gflags.h>

DEFINE_uint64(num_iter, 200, "Number of nonlinear iterations to run");
DEFINE_string(bal, "", "Input BAL problem");
DEFINE_string(csv, "", "Output CSV");
DEFINE_string(out, "", "Output BAL");
DEFINE_bool(cluster_jacobi, false, "");
DEFINE_bool(cluster_tridiagonal, false, "");
DEFINE_bool(multigrid, false, "");
DEFINE_bool(direct, false, "");
DEFINE_bool(explicit_schur, false, "");
DEFINE_bool(robust, false, "");
DEFINE_double(eta, 0.1, "ETA");
DEFINE_double(abstol, 1e-11, "Absolute cost tolerance");

using namespace ceres;
using namespace ceres::examples;

struct AbsTol: ceres::IterationCallback {
  AbsTol(double tol) : tol(tol){}
  double tol;
  ceres::CallbackReturnType operator()(const ceres::IterationSummary& summary) {
    if (summary.cost < tol) {
      return SOLVER_TERMINATE_SUCCESSFULLY;
    }
    return SOLVER_CONTINUE;
  }
};

int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  gflags::ParseCommandLineFlags(&argc, &argv, true);
  if(FLAGS_bal == "") {
    std::cout << "Must supply an input bal problem with --bal" << std::endl;
    return 1;
  }
  std::cout << "Reading BAL problem " << FLAGS_bal << std::endl;
  BALProblem bal_problem(FLAGS_bal, false);
  std::cout << "BAL Problem has " << bal_problem.num_cameras() << " cameras, " << bal_problem.num_points() << " points, and " << bal_problem.num_observations() << " observations." << std::endl;

  std::unique_ptr<ceres::IterationCallback> csv_callback;

  ceres::Problem problem;
  auto ordering = std::make_shared<ceres::ParameterBlockOrdering>();
  BuildProblem(bal_problem, &problem, ordering, FLAGS_robust);
  Solver::Options options;
  options.linear_solver_ordering = ordering;
  options.minimizer_type = ceres::TRUST_REGION;
  options.trust_region_strategy_type = ceres::LEVENBERG_MARQUARDT;
  options.minimizer_progress_to_stdout = true;
  options.gradient_tolerance = 1e-16;
  options.function_tolerance = 1e-16;
  options.max_num_iterations = FLAGS_num_iter;
  options.jacobi_scaling = true;
  options.max_linear_solver_iterations = 100000;
  options.num_threads = 1;
  options.use_explicit_schur_complement = FLAGS_explicit_schur;
  // options.trust_region_minimizer_iterations_to_dump = {0,1};
  // options.trust_region_problem_dump_directory = "/Users/tristan/Sync/Research/bundle_adjustment/ba-tao/dumps";
  std::string solver = "none";
  if(FLAGS_direct) {
    options.linear_solver_type = SPARSE_SCHUR;
    solver = "cholmod";
  } else {
    options.linear_solver_type = ITERATIVE_SCHUR;
    if(FLAGS_cluster_jacobi) {
      options.preconditioner_type = CLUSTER_JACOBI;
      solver = "visibility";
    } else if(FLAGS_cluster_tridiagonal) {
      options.preconditioner_type = CLUSTER_TRIDIAGONAL;
      solver = "visibility_tridiagonal";
    } else if(FLAGS_multigrid) {
      solver = "multigrid";
      options.preconditioner_type = JULIA_MULTIGRID;
    } else {
      if(FLAGS_explicit_schur) {
        solver = "pbjacobi_explicit";
      } else {
        solver = "pbjacobi_implicit";
      }
      options.preconditioner_type = SCHUR_JACOBI;
    }
  }
  if(FLAGS_csv != "") {
    csv_callback.reset(new CSVCallback(FLAGS_csv, solver, bal_problem, FLAGS_robust ? "huber" : "none"));
    options.callbacks = {csv_callback.get(), new AbsTol(FLAGS_abstol)};
  }
  options.eta = FLAGS_eta;
  options.sparse_linear_algebra_library_type = SUITE_SPARSE;
  // force ceres to run for maximum number of iterations
  options.function_tolerance = 0;
  options.gradient_tolerance = 0;
  options.parameter_tolerance = 0;
  Solver::Summary summary;
  std::cout << "Solving problem" << std::endl;
  Solve(options, &problem, &summary);
  std::cout << summary.FullReport() << "\n";

  if(FLAGS_out != "") {
    bal_problem.WriteToFile(FLAGS_out);
  }

  return 0;
}
