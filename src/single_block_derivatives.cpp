#include <ceres/ceres.h>
#include "snavely_reprojection_error.h"

// parameters: 2x observations, 9x camera, 3x point
extern "C" int compute_derivatives(const double * parameters, double* residuals, double* jacobian) {
  auto* cf = ceres::examples::SnavelyReprojectionError::Create(parameters[0], parameters[1]);
  const double* params[2];
  params[0] = parameters + 2;
  params[1] = parameters + 11;
  double* jac[2];
  jac[0] = jacobian;
  jac[1] = jacobian + 2*9;
  bool s = cf->Evaluate(params, residuals, jac);
  return s;
}
