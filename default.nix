{ stdenv, cmake, mpi, petsc, ceres-solver, eigen, pkgconfig }:

stdenv.mkDerivation rec {
  version = "0.0.1";
  name = "ba-tao";

  src = fetchGit ./.;
  nativeBuildInputs = [ cmake pkgconfig ];
  buildInputs = [ mpi petsc ceres-solver eigen ];

  enableParallelBuilding = true;
}
