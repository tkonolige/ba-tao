with (import <nixpkgs> { }).forceSystem "x86_64-linux" "x86_64";
let
  ba-tao = callPackage ./default.nix { mpi=mpich2; };
in
dockerTools.buildLayeredImage {
  name = "ba-tao-image";
  maxLayers = 128;

  contents = [ ba-tao ];

  config = {
    Env = ["LANG=en_US.UTF-8" ];
    config.Cmd = [ "${ba-tao}/bin/TaoBundleAdjustment" ];
    tag = "latest";
  };
}
