# Ceres TAO Bundle Adjustment

## Building

You will need a modified version of ceres solver from here:
https://github.com/tkonolige/ceres-solver.git. It needs to be configured with
`-DEXPORT_BUILD_DIR=On`.

```bash
mkdir build
cd build
cmake ../ -DCMAKE_BUILD_TYPE=RelWithDebInfo -DCeres_DIR=/path/to/ceres/build/
make -j 4
```

## Running

Sample data is available in `data/` (you need git lfs to grab it). Run with `build/bin/TaoBundleAdjustment -bal data/10_100k.bal -petscpartitioner_type simple`.

Recommended flags for ntr are `-pc_type fieldsplit -pc_fieldsplit_type schur -fieldsplit_point_mat_schur_complement_ainv_type blockdiag -fieldsplit_point_pc_type bjacobi -pc_fieldsplit_schur_precondition self -fieldsplit_camera_ksp_type cg -fieldsplit_camera_pc_type none -fieldsplit_camera_mat_schur_complement_ainv blockdiag`.

You can use the flags `-rot_sigma`, `-trans_sigma`, `-point_sigma` to randomly
perturb the camera rotations or translations or the point translations.
