#!/bin/zsh

mkdir -p $1
echo "-fieldsplit_camera_ksp_view_solution binary:$1/S-solution.petsc -fieldsplit_camera_ksp_view_rhs binary:$1/S-rhs.petsc -fieldsplit_camera_ksp_view_pmat binary:$1/S.petsc -ksp_view_diagonal_scale binary:$1/dscale.petsc -tao_view_solution binary:$1/solution.petsc"
