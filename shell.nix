{
  pkgs   ? import <nixpkgs> {},
  stdenv ? pkgs.stdenv,
  debugging ? false
} :

with pkgs;
 callPackage ./default.nix { petsc=pkgs.petsc.override {inherit debugging;}; mpi=mpich2;}
