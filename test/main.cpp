#include "jacobian_constructor.hpp"
#include <petsc.h>

PetscErrorCode test_observations_ordering(const BALProblem& problem) {
  PetscErrorCode ierr;
  PetscFunctionBegin;

  // Create new bal problem and fill it with sequential data
  auto p = problem.clone();
  for (PetscInt i = 0; i < p.num_parameters(); i++) {
    p.mutable_parameters()[i] = double(i);
  }
  for (PetscInt i = 0; i < p.num_observations() * 2; i++) {
    p.mutable_observations()[i] = double(i);
  }

  auto p2 = p.clone();

  std::unique_ptr<DistributedProblem> dp;
  DistributedProblem::create(MPI_COMM_WORLD, std::move(p), false, dp);

  std::map<std::pair<double, double>, double> obs;
  for (PetscInt i = 0; i < p2.num_observations(); i++) {
    obs[{p2.cameras()[p2.camera_index()[i] * p2.camera_block_size()],
         p2.points()[p2.point_index()[i] * p2.point_block_size()]}] =
        p2.observations()[i * 2];
  }

  double* params;
  Vec params_vec;
  ierr = DMGetCoordinatesLocal(dp->dm, &params_vec);CHKERRQ(ierr);
  ierr = VecGetArray(params_vec, &params);CHKERRQ(ierr);
  PetscSection local_section;
  ierr = DMGetCoordinateSection(dp->dm, &local_section);CHKERRQ(ierr);
  for (auto& c : dp->costs) {
    PetscInt c_off, p_off;
    ierr = PetscSectionGetOffset(local_section, c.camera, &c_off);CHKERRQ(ierr);
    ierr = PetscSectionGetOffset(local_section, c.point, &p_off);CHKERRQ(ierr);
    auto f = obs.find({params[c_off], params[p_off]});
    if (f == obs.end()) {
      SETERRQ2(PETSC_COMM_SELF, PETSC_ERR_USER,
               "Invalid edge created between camera %f and point %f\n",
               params[c_off], params[p_off]);
    }
    if (f->second != c.observed_x) {
      SETERRQ4(PETSC_COMM_SELF, PETSC_ERR_USER,
               "Invalid observation between camera %f and point %f. Expected: "
               "%f, actual: %f\n",
               params[c_off], params[p_off], f->second, c.observed_x);
    }
  }
  ierr = VecRestoreArray(params_vec, &params);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

PetscErrorCode test_bal_roundtrip(const BALProblem& bal) {
  PetscFunctionBegin;

  std::unique_ptr<DistributedProblem> prob;
  PetscErrorCode ierr;

  ierr = DistributedProblem::create(PETSC_COMM_WORLD, bal.clone(), false, prob);CHKERRQ(ierr);

  Tao tao;
  ierr = create_tao(prob.get(), &tao, nonstd::optional<BALProblem>(), std::unique_ptr<ceres::Problem>());CHKERRQ(ierr);

  BALProblem bal_copy = bal.clone();
  // overwrite bal_copy with bogus data
  for(size_t i = 0; i < bal_copy.num_parameters(); i++) {
    bal_copy.mutable_parameters()[i] = 0.0;
  }
  prob->copy_to_bal(tao, bal_copy);
  if(bal != bal_copy) {
    SETERRQ(PETSC_COMM_WORLD, PETSC_ERR_USER, "Cannot round trip BAL problem");
  }

  PetscFunctionReturn(0);
}

PetscErrorCode test_jacobian_nest(const DistributedProblem& problem) {
  PetscErrorCode ierr;
  Mat J, J_nest, J_sub, J_nest_sub;
  Mat submat;
  IS isrow[1], iscol[2];
  PetscReal norm;
  PetscBool multequal;
  MatType mtype;
  PetscFunctionBegin;

  ierr = problem.create_jacobian(&J, MATAIJ);CHKERRQ(ierr);
  ierr = problem.create_jacobian(&J_nest, MATNEST);CHKERRQ(ierr);

  ierr = MatGetType(J_nest, &mtype);CHKERRQ(ierr);
  if(std::string(mtype) != MATNEST) {
    SETERRQ1(PETSC_COMM_WORLD, PETSC_ERR_USER, "Nest matrix is actually %s", mtype);
  }

  ierr = MatNestGetISs(J_nest, isrow, iscol);CHKERRQ(ierr);

  ierr = MatMultEqual(J, J_nest, 100, &multequal);CHKERRQ(ierr);
  if(!multequal) {
    SETERRQ(PETSC_COMM_WORLD, PETSC_ERR_USER, "Nest and AIJ are not equal");
  }

  for(PetscInt i = 0; i < 2; i++) {
    ierr = MatNestGetSubMat(J_nest, 0, i, &J_nest_sub);CHKERRQ(ierr);
    ierr = MatCreateSubMatrix(J, isrow[0], iscol[i], MAT_INITIAL_MATRIX, &J_sub);CHKERRQ(ierr);
    ierr = MatAXPY(J_nest_sub, -1.0, J_sub, DIFFERENT_NONZERO_PATTERN);CHKERRQ(ierr);

    ierr = MatNorm(J_nest_sub, NORM_INFINITY, &norm);CHKERRQ(ierr);

    if(norm > 1e-8) {
      SETERRQ1(PETSC_COMM_WORLD, PETSC_ERR_USER, "Nest and AIJ matrices are different by %f", norm);
    }
  }

  PetscFunctionReturn(0);
}

PetscErrorCode test_nullspace(const DistributedProblem& problem) {
  PetscErrorCode ierr;
  PetscFunctionBegin;

  Vec param_vec;
  ierr = problem.get_parameter_vec(&param_vec);CHKERRQ(ierr);
  Mat J;
  ierr = problem.create_jacobian(&J);CHKERRQ(ierr);
  ierr = problem.compute(param_vec, J, NULL, NULL);CHKERRQ(ierr);
  MatNullSpace nullsp;
  IS iss[2];
  ierr = MatNestGetISs(J, NULL, iss);CHKERRQ(ierr);
  Vec camera_params;
  ierr = VecGetSubVector(param_vec, iss[0], &camera_params);CHKERRQ(ierr);
  ierr = problem.create_nullspace(camera_params, false, &nullsp);CHKERRQ(ierr);
  ierr = VecRestoreSubVector(param_vec, iss[0], &camera_params);CHKERRQ(ierr);

  // Create schur complement
  Mat camera_mat, point_mat;
  ierr = MatNestGetSubMat(J, 0, 0, &camera_mat);CHKERRQ(ierr);
  ierr = MatNestGetSubMat(J, 0, 1, &point_mat);CHKERRQ(ierr);
  Mat A00, A01, A11;
  ierr = MatTransposeMatMult(camera_mat, camera_mat, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &A11);CHKERRQ(ierr);
  ierr = MatTransposeMatMult(point_mat, point_mat, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &A00);CHKERRQ(ierr);
  ierr = MatTransposeMatMult(point_mat, camera_mat, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &A01);CHKERRQ(ierr);

  Mat A00i;
  ierr = MatCreate(PETSC_COMM_WORLD, &A00i);CHKERRQ(ierr);
  ierr = MatSetType(A00i, MATAIJ);CHKERRQ(ierr);
  PetscInt M,N,m,n;
  ierr = MatGetSize(A00,&M,&N);CHKERRQ(ierr);
  ierr = MatGetLocalSize(A00,&m,&n);CHKERRQ(ierr);
  ierr = MatSetSizes(A00i,m,n,M,N);CHKERRQ(ierr);
  ierr = MatSetUp(A00i);CHKERRQ(ierr);
  ierr = MatInvertBlockDiagonalMat(A00,A00i);CHKERRQ(ierr);
  Mat S;
  ierr = MatPtAP(A00i, A01, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &S);CHKERRQ(ierr);
  ierr = MatAYPX(S, -1, A11, SUBSET_NONZERO_PATTERN);CHKERRQ(ierr);

  Vec v;
  ierr = MatCreateVecs(S, NULL, &v);CHKERRQ(ierr);
  const Vec *ns;
  ierr = MatNullSpaceGetVecs(nullsp, NULL, &n, &ns);CHKERRQ(ierr);
  for(int i = 0; i < n; i++) {
    auto name = "nullspace" + std::to_string(i);
    ierr = MatMult(S, ns[i], v);CHKERRQ(ierr);
    PetscScalar norm;
    ierr = VecNorm(v, NORM_2, &norm);CHKERRQ(ierr);
    if(norm > PETSC_SQRT_MACHINE_EPSILON) {
      SETERRQ2(problem.comm, PETSC_ERR_USER, "Nullspace vector %d is not in nullspace of J (norm: %e)", i, norm);
    }
  }

  PetscFunctionReturn(0);
}

int main(int argc, char* argv[]) {
  PetscErrorCode ierr;
  PetscInitialize(&argc, &argv, NULL, "BA Tao test suite");
  char bal[PETSC_MAX_PATH_LEN];
  bal[0] = '\0';
  PetscBool bal_flag = PETSC_FALSE;

  ierr = PetscOptionsBegin(PETSC_COMM_WORLD, NULL, "Test options", NULL);CHKERRQ(ierr);
  ierr = PetscOptionsString("-bal", "Input .bal file", "",
                            bal, bal,
                            PETSC_MAX_PATH_LEN, &bal_flag);CHKERRQ(ierr);
  ierr = PetscOptionsEnd();CHKERRQ(ierr);
  if(!bal_flag) {
    ierr = PetscPrintf(PETSC_COMM_WORLD, "Error: no bal file specified\n");CHKERRQ(ierr);
    std::exit(1);
  }
  {

    BALProblem problem(bal, false);
    ierr = test_observations_ordering(problem);CHKERRQ(ierr);
    MPI_Barrier(MPI_COMM_WORLD);
    // ierr = test_bal_roundtrip(problem);CHKERRQ(ierr);
    // MPI_Barrier(MPI_COMM_WORLD);

    std::unique_ptr<DistributedProblem> dist_problem;
    ierr = DistributedProblem::create(PETSC_COMM_WORLD, std::move(problem), false, dist_problem);CHKERRQ(ierr);
    ierr = test_jacobian_nest(*dist_problem);CHKERRQ(ierr);
    MPI_Barrier(MPI_COMM_WORLD);

    ierr = test_nullspace(*dist_problem);CHKERRQ(ierr);
    MPI_Barrier(MPI_COMM_WORLD);
  }

  PetscFinalize();
}
