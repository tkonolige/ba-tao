cmake_minimum_required(VERSION 3.9)

project(TaoBundleAdjustment VERSION 0.1 LANGUAGES C CXX)

# put executables in bin
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

# Put FindPackage modules and others in cmake/
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/")

find_package(PETSc REQUIRED)
find_package(MPI REQUIRED)
find_package(Ceres REQUIRED)
find_package(Eigen3 REQUIRED)
find_package(gflags REQUIRED)

add_library(TaoBundleAdjustmentLib SHARED)
target_sources(TaoBundleAdjustmentLib PRIVATE src/bal_problem.cc)
target_include_directories(TaoBundleAdjustmentLib PUBLIC src/)
target_compile_features(TaoBundleAdjustmentLib PUBLIC cxx_std_14)
target_link_libraries(TaoBundleAdjustmentLib PUBLIC ceres Eigen3::Eigen)

add_executable(TaoBundleAdjustment src/main.cpp)
target_link_libraries(TaoBundleAdjustment PRIVATE TaoBundleAdjustmentLib PETSc::PETSc MPI::MPI_CXX MPI::MPI_C )

add_executable(CeresBundleAdjustment src/ceres_main.cpp)
target_include_directories(CeresBundleAdjustment PRIVATE ${GFLAGS_INCLUDE_DIRS})
target_link_libraries(CeresBundleAdjustment PRIVATE ceres TaoBundleAdjustmentLib ${GFLAGS_LIBRARIES})

add_library(derivatives SHARED src/single_block_derivatives.cpp)
target_link_libraries(derivatives PRIVATE ceres Eigen3::Eigen)

install(TARGETS TaoBundleAdjustment RUNTIME DESTINATION bin)
install(TARGETS CeresBundleAdjustment RUNTIME DESTINATION bin)
install(TARGETS TaoBundleAdjustmentLib ARCHIVE DESTINATION lib)
install(TARGETS derivatives ARCHIVE DESTINATION lib)

enable_testing()
add_subdirectory(test)
