find_package(PkgConfig REQUIRED)
if(DEFINED ENV{PETSC_DIR})
  set(ENV{PKG_CONFIG_PATH} "$ENV{PETSC_DIR}/$ENV{PETSC_ARCH}/lib/pkgconfig:$ENV{PKG_CONFIG_PATH}" )
  set(ENV{PKG_CONFIG_PATH} "$ENV{PETSC_DIR}/lib/pkgconfig:$ENV{PKG_CONFIG_PATH}" )
endif()
pkg_check_modules(PETSC IMPORTED_TARGET PETSc)

include (FindPackageHandleStandardArgs)
find_package_handle_standard_args (PETSc
  FAIL_MESSAGE "PETSc could not be found. Please set PETSC_ARCH and PETSC_DIR in your environment."
  REQUIRED_VARS PETSC_FOUND)

if(PETSC_FOUND)
  # Check that the petsc libraries actually exist
  # TODO: use the existing PkgConfig::PETSC target instead
  add_library(PETSc::PETSc INTERFACE IMPORTED)
  if(PETSC_LINK_LIBRARIES)
    message(STATUS "Found PETSc Libraries: ${PETSC_LINK_LIBRARIES}")
    set_target_properties(PETSc::PETSc PROPERTIES INTERFACE_INCLUDE_DIRECTORIES "${PETSC_INCLUDE_DIRS}" INTERFACE_LINK_LIBRARIES "${PETSC_LINK_LIBRARIES}")
  elseif(PETSC_LIBRARIES AND PETSC_LIBRARY_DIRS)
    message(STATUS "Found PETSc Libraries: ${PETSC_LIBRARY_DIRS} ${PETSC_LIBRARIES}")
    set_target_properties(PETSc::PETSc PROPERTIES INTERFACE_INCLUDE_DIRECTORIES "${PETSC_INCLUDE_DIRS}" INTERFACE_LINK_LIBRARIES "${PETSC_LIBRARIES}" INTERFACE_LINK_DIRECTORIES "${PETSC_LIBRARY_DIRS}")
  elseif(PETSC_LINK_LIBRARIES)
    message(STATUS "Found PETSc Libraries: ${PETSC_LIBRARIES}")
    set_target_properties(PETSc::PETSc PROPERTIES INTERFACE_INCLUDE_DIRECTORIES "${PETSC_INCLUDE_DIRS}" INTERFACE_LINK_LIBRARIES "${PETSC_LIBRARIES}")
  endif()
endif()
